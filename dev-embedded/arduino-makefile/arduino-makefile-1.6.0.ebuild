# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Makefile for Arduino sketches.
Defines the workflows for compiling code, flashing it to Arduino and even communicating through Serial."
HOMEPAGE="https://hardwarefun.com/tutorials/compiling-arduino-sketches-using-makefile"
SRC_URI="https://github.com/sudar/${PN}/archive/${PV}.tar.gz"
S=${WORKDIR}/"Arduino-Makefile-${PV}"

LICENSE="LGPL"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="examples"

DEPEND=""
RDEPEND="dev-embedded/arduino dev-python/pyserial sys-devel/make"
BDEPEND=""

# src_test() {
# 	./tests/script/runtests.sh
# }

src_install() {
	dobin bin/*

	insinto /usr/share/arduino/
	doins *.mk arduino-mk-vars.md
	dosym /usr/share/arduino/Arduino.mk /usr/include/Arduino.mk

	doman ard-reset-arduino.1

	dodoc README.md HISTORY.md
	if use examples; then
		docompress -x /usr/share/doc/${PF}/examples
		dodoc -r examples
	fi
}
