# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Arduino core for the ESP32"
HOMEPAGE="https://github.com/espressif/arduino-esp32"
SRC_URI="https://github.com/espressif/${PN}/archive/${PV}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND} dev-embedded/esptool"
BDEPEND=""


src_install() {
	insinto /usr/share/arduino/hardware/espressif/esp32/
	doins boards.txt platform.txt programmers.txt
	doins -r tools/ cores/ libraries/ variants/
}
