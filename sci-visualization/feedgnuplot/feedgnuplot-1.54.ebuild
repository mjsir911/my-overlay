# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit perl-module

DESCRIPTION="Tool to plot realtime and stored data from the commandline, using gnuplot"
HOMEPAGE="https://github.com/dkogan/feedgnuplot"
SRC_URI="https://github.com/dkogan/feedgnuplot/archive/v1.54.tar.gz"

LICENSE="|| ( GPL-1 Artistic )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="dev-perl/String-ShellQuote dev-perl/List-MoreUtils dev-perl/IPC-Run"
