# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit golang-build

DESCRIPTION="A terminal UI for tshark, inspired by Wireshark"
HOMEPAGE="https://github.com/gcla/termshark"
SRC_URI="https://github.com/gcla/${PN}/archive/v${PV}.tar.gz"

EGO_PN="github.com/gcla/${PN}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	dev-go/go-crypto:=
	dev-go/go-text:=
	dev-go/go-sys:=
	dev-go/go-net:=
	dev-go/go-tools:=
	net-analyzer/wireshark[dumpcap,pcap,tshark]"

DEPEND="${RDEPEND}"
BDEPEND="dev-lang/go"

src_unpack() {
	default
	mkdir -p ${P}/src/${EGO_PN}
	mv ${P}/* ${P}/src/${EGO_PN}
}

