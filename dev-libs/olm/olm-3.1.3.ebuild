# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{6..9} )
DISTUTILS_OPTIONAL=1

inherit cmake-utils distutils-r1

DESCRIPTION="Implementation of the olm and megolm cryptographic ratchets "
HOMEPAGE="https://gitlab.matrix.org/matrix-org/olm"
SRC_URI="https://gitlab.matrix.org/matrix-org/${PN}/-/archive/${PV}/${P}.tar.gz"


LICENSE="Apache 2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="python test"
RESTRICT="!test? ( test )"
REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"

DEPEND="
	python? (
		${PYTHON_DEPS}
		dev-python/setuptools[${PYTHON_USEDEP}]
		test? (
			dev-python/pytest
			dev-python/pytest-benchmark
		)
	)
"

RDEPEND="
	python? (
		>=dev-python/cffi-1.0.0[${PYTHON_USEDEP}]
		dev-python/future[${PYTHON_USEDEP}]
	)
"

BDEPEND="
	python? (
		>=dev-python/cffi-1.0.0[${PYTHON_USEDEP}]
	)
"


src_test() {
	default
	if use python ; then
		cd python || die
		make test
	fi
}


src_compile() {
	default
	if use python ; then
		cd python || die
		distutils-r1_src_compile
	fi
}

src_install() {
	default
	if use python ; then
		cd python || die
		distutils-r1_src_install
	fi
}
