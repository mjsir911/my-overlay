# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="Helper for unix password manager to plug in to awscli credential_process"
HOMEPAGE="https://gitlab.com/mjsir911/pass-awscli"
EGIT_REPO_URI="https://gitlab.com/mjsir911/${PN}.git"

LICENSE=""
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="app-admin/pass"

src_install() {
	exeinto /usr/lib/password-store/extensions
	doexe aws.bash
}
