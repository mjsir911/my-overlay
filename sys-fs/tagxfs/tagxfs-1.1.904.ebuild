# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Tagxfs is a semantic file system"
HOMEPAGE="http://tagxfs.sourceforge.net/"
SRC_URI="https://sourceforge.net/projects/${PN}/files/${PN}-1.1-904.tar.gz"

LICENSE="Apache-1.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

S="$WORKDIR/${PN}-1.1-904"

DEPEND=""
RDEPEND="
	>=sys-fs/fuse-2.7.0
"
BDEPEND=""
