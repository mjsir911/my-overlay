# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Nyxt - the internet on your terms."
HOMEPAGE="https://nyxt.atlas.engineer"
SRC_URI="https://github.com/atlas-engineer/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+gtk"

# See SOURCES.org for handy list
# Also next.asd because SOURCES.org is outdated
# 	dev-lisp/asdf
# 	>=dev-lisp/uiop-3.3.2
# 	dev-lisp/trivial-features
# 	dev-lisp/prove

BDEPEND="
	>=dev-lisp/sbcl-1.5.0
	gtk? (
		dev-lisp/cl-cffi-gtk
		dev-lisp/cl-gobject-introspection
		dev-lisp/cl-webkit2
	)

	dev-lisp/alexandria
	dev-lisp/bordeaux-threads
	dev-lisp/calispel
	dev-lisp/cl-css
	=dev-lisp/cl-containers-9999
	dev-lisp/cl-custom-hash-table
	dev-lisp/cl-html-diff
	dev-lisp/cl-json
	dev-lisp/cl-markup
	dev-lisp/cl-ppcre
	dev-lisp/cl-ppcre-unicode
	=dev-lisp/cl-prevalence-9999
	dev-lisp/closer-mop
	dev-lisp/cluffer
	dev-lisp/dexador
	dev-lisp/enchant
	=dev-lisp/fset-9999
	dev-lisp/hu_dwim_defclass-star
	dev-lisp/iolib
	dev-lisp/local-time
	dev-lisp/log4cl
	dev-lisp/lparallel
	dev-lisp/mk-string-metrics
	dev-lisp/moptilities
	dev-lisp/named-readtables
	dev-lisp/parenscript
	dev-lisp/plump
	dev-lisp/quri
	dev-lisp/serapeum
	dev-lisp/str
	dev-lisp/swank
	dev-lisp/trivia
	dev-lisp/trivial-clipboard
	dev-lisp/trivial-features
	dev-lisp/trivial-package-local-nicknames
	dev-lisp/trivial-types
	dev-lisp/unix-opts
"

DEPEND=""
RDEPEND="${DEPEND}"


src_prepare() {
	default
	echo ${PV} > version
}

src_compile() {
# 	common-lisp-export-impl-args "$(common-lisp-find-lisp-impl)"
# 	ASDF_OUTPUT_TRANSLATIONS=${S}:${S} ${CL_BINARY} ${CL_NORC} \
# 		${CL_EVAL} '(require :asdf)' \
# 		${CL_LOAD} next.asd \
# 		${CL_EVAL} '(asdf:make "next")'
	emake NYXT_INTERNAL_QUICKLISP=false all
}
# 
src_install() {
	emake DESTDIR=${D} PREFIX=/usr NYXT_INTERNAL_QUICKLISP=false install
	einstalldocs
}
