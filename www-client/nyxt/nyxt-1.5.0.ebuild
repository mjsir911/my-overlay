# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Nyxt - the internet on your terms."
HOMEPAGE="https://nyxt.atlas.engineer"
SRC_URI="https://github.com/atlas-engineer/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

# See SOURCES.org for handy list
# Also next.asd because SOURCES.org is outdated
BDEPEND="
	virtual/commonlisp
	net-libs/webkit-gtk

	dev-lisp/asdf
	>=dev-lisp/uiop-3.3.2
	dev-lisp/trivial-features
	dev-lisp/prove

	dev-lisp/alexandria
	dev-lisp/bordeaux-threads
	dev-lisp/cl-annot
	dev-lisp/cl-ansi-text
	dev-lisp/cl-css
	dev-lisp/cl-json
	dev-lisp/cl-markup
	dev-lisp/cl-ppcre
	dev-lisp/cl-ppcre-unicode
	dev-lisp/cl-prevalence
	dev-lisp/closer-mop
	dev-lisp/dbus
	dev-lisp/dexador
	dev-lisp/ironclad
	dev-lisp/local-time
	dev-lisp/log4cl
	dev-lisp/lparallel
	dev-lisp/mk-string-metrics
	dev-lisp/parenscript
	dev-lisp/quri
	dev-lisp/serapeum
	dev-lisp/cl-sqlite
	dev-lisp/str
	dev-lisp/plump
	dev-lisp/swank
	dev-lisp/trivia
	dev-lisp/trivial-clipboard
	dev-lisp/trivial-types
	dev-lisp/unix-opts
"

DEPEND=""
RDEPEND="${DEPEND}"


src_prepare() {
	default
	echo ${PV} > version
}

src_compile() {
	common-lisp-export-impl-args "$(common-lisp-find-lisp-impl)"
	ASDF_OUTPUT_TRANSLATIONS=${S}:${S} ${CL_BINARY} ${CL_NORC} \
		${CL_EVAL} '(require :asdf)' \
		${CL_LOAD} next.asd \
		${CL_EVAL} '(asdf:make "next")'
	emake gtk-webkit
}

src_install() {
	emake DESTDIR=${D} PREFIX=/usr --assume-old next install
}
