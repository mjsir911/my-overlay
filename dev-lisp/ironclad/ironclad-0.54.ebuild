# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="A cryptographic toolkit written in Common Lisp"
# looks like this is an up to date fork used by quicklisp
HOMEPAGE="https://github.com/sharplispers/ironclad"
SRC_URI="https://github.com/sharplispers/ironclad/archive/refs/tags/v0.54.tar.gz -> ${P}.tar.gz"

LICENSE="BSD-3-Clause"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="sbcl test"

DEPEND="
sbcl? ( dev-lisp/sb-rotate-byte dev-lisp/sb-posix )
dev-lisp/bordeaux-threads
test? ( dev-lisp/rt )
"
RDEPEND="${DEPEND}"
BDEPEND=""

HTML_DOCS="doc/${PN}.html"
