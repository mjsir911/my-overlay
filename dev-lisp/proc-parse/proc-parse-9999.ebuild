# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Procedural vector parser"
HOMEPAGE="https://github.com/fukamachi/proc-parse"
EGIT_REPO_URI="https://github.com/fukamachi/${PN}.git"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS=""

RDEPEND="dev-lisp/alexandria dev-lisp/babel"
