# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Misc utility functions"
HOMEPAGE="http://www.sf.net/projects/com-lisp-utils/"
SRC_URI="mirror://sourceforge/com-lisp-utils/com-lisp-utils-${PV}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND=""

S=${WORKDIR}/com-lisp-utils-${PV}/rsm
