# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Pattern Matcher Compatible with Optima"
HOMEPAGE="https://github.com/guicho271828/trivia"
EGIT_REPO_URI="https://github.com/guicho271828/${PN}.git"

LICENSE="LLGPL"
SLOT="0"
KEYWORDS=""

DEPEND="
	dev-lisp/iterate
	dev-lisp/alexandria
	dev-lisp/lisp-namespace
	dev-lisp/closer-mop
	dev-lisp/trivial-cltl2
"
# Depends on trivia
PDEPEND="dev-lisp/type-i"
RDEPEND="${DEPEND}"
BDEPEND=""
