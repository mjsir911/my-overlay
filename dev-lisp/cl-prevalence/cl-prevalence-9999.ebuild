# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="In memory database system for Common Lisp"
HOMEPAGE="https://github.com/40ants/cl-prevalence"
EGIT_REPO_URI="https://github.com/40ants/${PN}.git"

LICENSE="LLGPL-2.1"
SLOT="0"
KEYWORDS=""

RDEPEND="dev-lisp/s-xml =dev-lisp/s-sysdeps-9999"
