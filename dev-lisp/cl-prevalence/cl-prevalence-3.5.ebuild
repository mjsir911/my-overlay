# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="In memory database system for Common Lisp"
HOMEPAGE="https://github.com/40ants/cl-prevalence"
SRC_URI="https://github.com/40ants/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LLGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="test"

DEPEND="
	dev-lisp/s-xml dev-lisp/s-sysdeps
	test? ( dev-lisp/fiveam )
"
RDEPEND="${DEPEND}"
BDEPEND=""
