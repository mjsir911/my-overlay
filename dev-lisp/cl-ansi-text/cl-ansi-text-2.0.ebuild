# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Enables ANSI colors for printing."
HOMEPAGE="https://github.com/pnathan/cl-ansi-text"
SRC_URI="https://github.com/pnathan/cl-ansi-text/archive/refs/tags/v2.0.tar.gz -> ${P}.tar.gz"

LICENSE="null"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
dev-lisp/cl-colors2
dev-lisp/alexandria
"
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}/cl-ansi-text-2.0
