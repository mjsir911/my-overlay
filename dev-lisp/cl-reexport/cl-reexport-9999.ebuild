# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Reexport external symbols in other packages."
HOMEPAGE="https://github.com/takagi/cl-reexport"
EGIT_REPO_URI="https://github.com/takagi/${PN}.git"

LICENSE="LLGPL"
SLOT="0"
KEYWORDS=""

RDEPEND="dev-lisp/alexandria"
