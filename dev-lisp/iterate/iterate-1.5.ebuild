# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Jonathan Amsterdam's iterator/gatherer/accumulator facility"
HOMEPAGE="https://common-lisp.net/project/iterate/"
SRC_URI="https://common-lisp.net/project/${PN}/releases/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="!dev-lisp/cl-${PN}"
RDEPEND="${DEPEND}"
BDEPEND=""

DOCS="doc/tex/*.pdf"
