# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

MY_PN=${PN//+/-plus-}
DESCRIPTION="Common Lisp bindings to OpenSSL."
HOMEPAGE="https://github.com/cl-plus-ssl/cl-plus-ssl"
EGIT_REPO_URI="https://github.com/${MY_PN}/${MY_PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

IUSE="sbcl"

RDEPEND="
	dev-lisp/cffi
	dev-lisp/trivial-gray-streams
	dev-lisp/flexi-streams
	sbcl? ( dev-lisp/sb-posix )
	dev-lisp/bordeaux-threads
	dev-lisp/trivial-garbage
	dev-lisp/uiop
	dev-lisp/usocket
	dev-lisp/alexandria
	dev-lisp/trivial-features
"
