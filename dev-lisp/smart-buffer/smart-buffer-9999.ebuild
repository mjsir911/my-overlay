# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Smart octets buffer."
HOMEPAGE="https://github.com/fukamachi/smart-buffer"
EGIT_REPO_URI="https://github.com/fukamachi/${PN}.git"

LICENSE="BSD"
SLOT="0"
KEYWORDS=""

RDEPEND="
	dev-lisp/xsubseq
	dev-lisp/flexi-streams
	dev-lisp/uiop
"
