# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Parenscript is a translator from an extended subset of Common Lisp to JavaScript"
HOMEPAGE="https://common-lisp.net/project/parenscript/"
SRC_URI="https://common-lisp.net/project/${PN}/release/${P}.tgz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="test"

DEPEND="
	!dev-lisp/cl-${PN}
	!dev-lisp/cl-${PN}-darcs
	!dev-lisp/${PN}-darcs
	dev-lisp/cl-ppcre dev-lisp/anaphora dev-lisp/named-readtables
	test? ( dev-lisp/fiveam dev-lisp/cl-js )
"
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}/Parenscript-2.7.1 # why capitalize ;-;

CLSYSTEMS="${PN}"

HTML_DOCS="docs/*.html"

DOCS="README contributors docs/introduction.lisp"

src_install() {
	if use test; then
		CLSYSTEMS="${CLSYSTEMS} ${PN}.tests"
	fi
	common-lisp-3_src_install
}
