# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="CSP-like channels for common lisp"
HOMEPAGE="https://www.thoughtcrime.us/software/calispel/"
SRC_URI="http://www.thoughtcrime.us/software/${PN}/${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="
=dev-lisp/jpl-queues-0.1
=dev-lisp/jpl-util-0.2
=dev-lisp/eager-future-0.1
dev-lisp/bordeaux-threads
"
