# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="A few simple tools to help you with documenting your library."
HOMEPAGE="http://shinmera.github.io/documentation-utils/"
EGIT_REPO_URI="https://github.com/Shinmera/${PN}.git"

LICENSE="ZLIB"
SLOT="0"
KEYWORDS=""

RDEPEND="dev-lisp/trivial-indent"
