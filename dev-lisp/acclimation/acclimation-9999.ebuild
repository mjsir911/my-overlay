# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Tools for internationalization of Common Lisp programs."
HOMEPAGE="https://github.com/robert-strandh/Acclimation"
EGIT_REPO_URI="https://github.com/robert-strandh/Acclimation.git"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS=""

RDEPEND=""
