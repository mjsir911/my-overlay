# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="A few different kinds of queues, with optional multithreading synchronization."
HOMEPAGE="https://www.thoughtcrime.us/software/jpl-queues/"
SRC_URI="http://www.thoughtcrime.us/software/${PN}/${P}.tar.gz"

LICENSE="ISC-style permissive"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="dev-lisp/bordeaux-threads =dev-lisp/jpl-util-0.2"
