# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="This system provides the hooks extension point mechanism
(as known, e.g., from GNU Emacs)."
HOMEPAGE="https://launchpad.net/cl-hooks/trunk"
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
