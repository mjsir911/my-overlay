# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="The Common Foreign Function Interface"
HOMEPAGE="http://common-lisp.net/project/cffi"
SRC_URI="https://github.com/cffi/cffi/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="abcl"

RDEPEND="
	dev-lisp/uiop
	dev-lisp/alexandria
	dev-lisp/trivial-features
	dev-lisp/babel
"
