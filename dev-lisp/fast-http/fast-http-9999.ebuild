# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="A fast HTTP request/response parser for Common Lisp."
HOMEPAGE="https://github.com/fukamachi/fast-http"
EGIT_REPO_URI="https://github.com/fukamachi/${PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

RDEPEND="
	dev-lisp/alexandria
	dev-lisp/cl-utilities
	dev-lisp/proc-parse
	dev-lisp/babel
	dev-lisp/xsubseq
	dev-lisp/smart-buffer
"
