# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Efficient way to use \"subseq\"s in Common Lisp"
HOMEPAGE="https://github.com/fukamachi/xsubseq"
EGIT_REPO_URI="https://github.com/fukamachi/${PN}.git"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS=""

IUSE="sbcl"

RDEPEND="sbcl? ( dev-lisp/sb-cltl2 )"
