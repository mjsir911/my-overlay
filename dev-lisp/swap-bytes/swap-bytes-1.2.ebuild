# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Efficient endianness conversion for SBCL"
HOMEPAGE="https://github.com/sionescu/swap-bytes"
SRC_URI="https://github.com/sionescu/swap-bytes/archive/refs/tags/v1.2.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-lisp/trivial-features"
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}/${PN}-1.2

src_install() {
	common-lisp-3_src_install
	common-lisp-install-sources -t all version.sexp
}
