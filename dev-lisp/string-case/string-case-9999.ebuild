# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Excessive macro that generates decision trees for small sets of strings"
HOMEPAGE="http://pvk.ca/Blog/Lisp/string_case_bis.html"
EGIT_REPO_URI="https://github.com/pkhuong/${PN}.git"

LICENSE="BSD"
SLOT="0"
KEYWORDS=""

RDEPEND=""
