# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Python-like Annotation Syntax for Common Lisp"
HOMEPAGE="https://github.com/m2ym/cl-annot"
EGIT_REPO_URI="https://github.com/m2ym/${PN}.git"

LICENSE="LLGPL"
SLOT="0"
KEYWORDS=""

RDEPEND=""
