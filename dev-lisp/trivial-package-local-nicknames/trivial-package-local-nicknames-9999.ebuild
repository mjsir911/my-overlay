# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Common Lisp PLN compatibility library."
HOMEPAGE="https://github.com/phoe/trivial-package-local-nicknames"
EGIT_REPO_URI="https://github.com/phoe/${PN}.git"

LICENSE="Unlicense"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND=""
