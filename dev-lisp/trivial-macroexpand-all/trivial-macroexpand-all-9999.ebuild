# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="macroexpand-all function that calls each implementation's equivalent"
HOMEPAGE="https://github.com/cbaggers/trivial-macroexpand-all"
EGIT_REPO_URI="https://github.com/cbaggers/${PN}.git"

LICENSE="Unlicense"
SLOT="0"
KEYWORDS=""

IUSE="sbcl"

RDEPEND="sbcl? ( dev-lisp/sb-cltl2 )"
