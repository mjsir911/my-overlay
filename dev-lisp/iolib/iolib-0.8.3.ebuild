# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Common Lisp I/O library"
HOMEPAGE="http://common-lisp.net/project/iolib/"
# SRC_URI="https://common-lisp.net/project/${PN}/files/${P}.tar.gz"
SRC_URI="https://github.com/sionescu/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	dev-libs/libfixposix

	dev-lisp/babel
	dev-lisp/cffi
	dev-lisp/bordeaux-threads
	dev-lisp/idna
	dev-lisp/swap-bytes
	dev-lisp/trivial-features
	dev-lisp/alexandria
	dev-lisp/split-sequence
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	common-lisp-3_src_install
	common-lisp-install-sources -t all version.sexp
	doheader src/grovel/grovel-common.h
	einstalldocs
}
