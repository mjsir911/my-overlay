# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Define efficient global variables in Common Lisp."
HOMEPAGE="https://github.com/lmj/global-vars"
EGIT_REPO_URI="https://github.com/lmj/${PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

RDEPEND=""
