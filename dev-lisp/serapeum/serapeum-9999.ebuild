# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Utilities beyond Alexandria"
HOMEPAGE="https://github.com/ruricolist/serapeum"
EGIT_REPO_URI="https://github.com/ruricolist/${PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

RDEPEND="
	dev-lisp/alexandria
	dev-lisp/trivia
	dev-lisp/uiop
	dev-lisp/split-sequence
	dev-lisp/string-case
	dev-lisp/parse-number
	dev-lisp/trivial-garbage
	dev-lisp/bordeaux-threads
	dev-lisp/named-readtables
	dev-lisp/fare-quasiquote
	dev-lisp/parse-declarations
	dev-lisp/introspect-environment
	dev-lisp/trivial-cltl2
	dev-lisp/global-vars
	dev-lisp/trivial-file-size
	dev-lisp/trivial-macroexpand-all
	dev-lisp/babel
"
