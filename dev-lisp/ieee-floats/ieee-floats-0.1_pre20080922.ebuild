# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Common Lisp IEEE-754 float en- and decoding"
HOMEPAGE="http://www.common-lisp.net/project/ieee-floats"
SRC_URI="http://common-lisp.net/~sionescu/files/${P}.tar.bz2"

LICENSE="ZLIB"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

DEPEND="test? ( dev-lisp/fiveam )"
RDEPEND="${DEPEND}"
BDEPEND=""

HTMLDOCS="doc/*"
