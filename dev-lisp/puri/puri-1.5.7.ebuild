# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Portable Universal Resource Indentifier Library"
HOMEPAGE="http://files.kpe.io/puri/"
SRC_URI="http://files.kpe.io/${PN}/${P}.tar.gz"

LICENSE="LLGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="test"

DEPEND="test? ( dev-lisp/ptester )"
RDEPEND="${DEPEND}"
BDEPEND=""
