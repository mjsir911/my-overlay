# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Binding to GObjectIntrospection"
HOMEPAGE="https://github.com/andy128k/cl-gobject-introspection"
EGIT_REPO_URI="https://github.com/andy128k/cl-gobject-introspection.git"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="
	dev-lisp/alexandria
	dev-lisp/cffi
	dev-lisp/iterate
	dev-lisp/trivial-garbage

	dev-libs/gobject-introspection
"
