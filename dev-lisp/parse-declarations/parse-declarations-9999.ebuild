# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Library to parse and rebuild declarations."
HOMEPAGE="https://gitlab.common-lisp.net/parse-declarations/parse-declarations"
EGIT_REPO_URI="https://gitlab.common-lisp.net/${PN}/${PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

RDEPEND=""
