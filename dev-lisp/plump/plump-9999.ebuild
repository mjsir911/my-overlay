# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Practically Lenient and Unimpressive Markup Parser for Common Lisp"
HOMEPAGE="https://shinmera.github.io/plump"
EGIT_REPO_URI="https://github.com/Shinmera/plump.git"

LICENSE="ZLIB"
SLOT="0"
KEYWORDS=""

RDEPEND="dev-lisp/array-utils dev-lisp/documentation-utils"
