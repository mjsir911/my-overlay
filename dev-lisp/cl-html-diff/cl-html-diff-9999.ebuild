# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="
A Common Lisp library for generating a human-readable diff of two HTML documents."
HOMEPAGE="https://github.com/wiseman/cl-html-diff"
EGIT_REPO_URI="https://github.com/wiseman/cl-html-diff.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

RDEPEND="dev-lisp/cl-difflib"
