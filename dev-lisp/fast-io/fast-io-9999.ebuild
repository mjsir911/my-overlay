# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Fast octet-vector/stream I/O for Common Lisp"
HOMEPAGE="https://github.com/rpav/fast-io"
EGIT_REPO_URI="https://github.com/rpav/${PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

IUSE="+static-vectors"

RDEPEND="
dev-lisp/alexandria dev-lisp/trivial-gray-streams
static-vectors? ( dev-lisp/static-vectors )"
