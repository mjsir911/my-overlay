# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="no more discussion on lisp-1 vs lisp-2. THIS IS LISP-N."
HOMEPAGE="https://github.com/guicho271828/lisp-namespace"
EGIT_REPO_URI="https://github.com/guicho271828/${PN}.git"

LICENSE="LLGPL"
SLOT="0"
KEYWORDS=""

RDEPEND="dev-lisp/alexandria"
