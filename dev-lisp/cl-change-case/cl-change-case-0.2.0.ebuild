# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Convert strings between camelCase, param-case, snake_case and more"
HOMEPAGE="https://github.com/rudolfochrist/cl-change-case"
SRC_URI="https://github.com/rudolfochrist/cl-change-case/archive/0.2.0.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="test"

DEPEND="dev-lisp/cl-ppcre dev-lisp/cl-ppcre-unicode test? ( dev-lisp/fiveam )"
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}/${PN}-0.2.0

src_install() {
	common-lisp-3_src_install
	common-lisp-install-sources -t all version
}
