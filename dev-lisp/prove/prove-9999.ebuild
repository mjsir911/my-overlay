# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Yet another unit testing framework for Common Lisp "
HOMEPAGE="https://github.com/fukamachi/prove"
# Just pull master
EGIT_REPO_URI="https://github.com/fukamachi/${PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

RDEPEND=""
