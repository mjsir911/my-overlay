# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="A very simple library to allow indentation hints for SWANK."
HOMEPAGE="https://github.com/Shinmera/trivial-indent"
EGIT_REPO_URI="https://github.com/Shinmera/${PN}.git"

LICENSE="ZLIB"
SLOT="0"
KEYWORDS=""

RDEPEND=""
