# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="A Common Lisp library of semi-standard utilities."
HOMEPAGE="https://common-lisp.net/project/cl-utilities/"
SRC_URI="http://www.common-lisp.net/project/${PN}/${P}.tar.gz"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

HTMLDOCS="doc/*"
