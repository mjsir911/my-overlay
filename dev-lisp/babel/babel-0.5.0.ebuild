# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

MY_P=${PN}_${PV}

DESCRIPTION="A charset encoding/decoding library."
HOMEPAGE="http://common-lisp.net/project/babel"
SRC_URI="http://common-lisp.net/project/${PN}/releases/${MY_P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	dev-lisp/trivial-features
	dev-lisp/alexandria
	dev-lisp/trivial-gray-streams
"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}"/${MY_P}
