# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="
SPLIT-SEQUENCE is a member of the Common Lisp Utilities family of programs, designed by community consensus.
"
HOMEPAGE="http://cliki.net/split-sequence"
SRC_URI="https://github.com/sharplispers/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="test"

DEPEND="!dev-lisp/cl-${PN} test? ( dev-lisp/fiveam )"
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	common-lisp-3_src_install
	common-lisp-install-sources -t all version.sexp
}
