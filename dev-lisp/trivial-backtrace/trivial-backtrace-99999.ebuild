# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="A simple library for generating a backtrace portably."
HOMEPAGE="http://common-lisp.net/project/trivial-backtrace
	https://github.com/gwkkwg/trivial-backtrace"
EGIT_REPO_URI="https://github.com/gwkkwg/trivial-backtrace.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

IUSE="test"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

CLSYSTEMS="${PN}"

src_install() {
	if use test; then
		CLSYSTEMS="${CLSYSTEMS} ${PN}-test"
	fi
	common-lisp-3_src_install
}
