# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="ASDF extension to provide support for file-local variables"
HOMEPAGE="http://www.lrde.epita.fr/~didier/software/lisp/misc.php#asdf-flv"
SRC_URI="https://www.lrde.epita.fr/~didier/software/lisp/${PN}/attic/${P}.tar.gz"

LICENSE="FSFAP"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}/${PN}-version-2.1
