# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Tiny Common Lisp library to detect mime types in files."
HOMEPAGE="http://shinmera.github.io/trivial-mimes/"
EGIT_REPO_URI="https://github.com/Shinmera/${PN}.git"

LICENSE="ZLIB"
SLOT="0"
KEYWORDS=""

RDEPEND=""
