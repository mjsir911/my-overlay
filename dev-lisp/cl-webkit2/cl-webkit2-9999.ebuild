# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

MY_PN="${PN/2/}"
MY_P=${MY_PN}-${PV}
DESCRIPTION="A binding to WebKitGTK+ for Common Lisp"
HOMEPAGE="https://github.com/joachifm/cl-webkit"
EGIT_REPO_URI="https://github.com/joachifm/${MY_PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

RDEPEND="
dev-lisp/cffi
dev-lisp/cl-cffi-gtk

net-libs/webkit-gtk:4
"
