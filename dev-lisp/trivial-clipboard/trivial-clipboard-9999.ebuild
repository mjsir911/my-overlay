# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="trivial-clipboard let access system clipboard."
HOMEPAGE="https://github.com/snmsts/trivial-clipboard"
EGIT_REPO_URI="https://github.com/snmsts/${PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

RDEPEND="dev-lisp/uiop"
