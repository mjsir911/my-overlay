# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Modern markup (HTML) generation library for Common Lisp"
HOMEPAGE="https://github.com/arielnetworks/cl-markup"
EGIT_REPO_URI="https://github.com/arielnetworks/${PN}.git"

LICENSE="LLGPL"
SLOT="0"
KEYWORDS=""

RDEPEND=""
