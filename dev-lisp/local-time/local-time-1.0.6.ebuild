# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Time manipulation library for Common Lisp"
HOMEPAGE="http://common-lisp.net/project/local-time/"
SRC_URI="https://github.com/dlowe-net/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD-1"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-lisp/cl-fad"
RDEPEND="${DEPEND}"
BDEPEND=""

# TODO use-system-zonedata
# TODO also compile docs
