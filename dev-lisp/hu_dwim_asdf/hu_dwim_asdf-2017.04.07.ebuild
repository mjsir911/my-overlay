# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

MY_PN=${PN//_/.}
MY_PV=${PV//./-}
MY_P=${MY_PN}-${MY_PV}

DESCRIPTION="A Common Lisp DEFCLASS* for less boilerplate "
HOMEPAGE="https://github.com/hu-dwim/hu.dwim.asdf"
SRC_URI="https://github.com/hu-dwim/${MY_PN}/archive/${MY_PV}.tar.gz -> ${P}.tar.gz"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="dev-lisp/asdf dev-lisp/uiop"

S=${WORKDIR}/${MY_P}
