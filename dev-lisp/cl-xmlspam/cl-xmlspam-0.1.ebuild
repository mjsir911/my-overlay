# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Streaming pattern matching for XML"
HOMEPAGE="https://common-lisp.net/project/cl-xmlspam"
SRC_URI="http://common-lisp.net/project/cl-xmlspam/cl-xmlspam.tgz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-lisp/cxml dev-lisp/cl-ppcre"
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}/${PN}
