# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="HTTP cookie manager"
HOMEPAGE="https://github.com/fukamachi/cl-cookie"
EGIT_REPO_URI="https://github.com/fukamachi/${PN}"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS=""

RDEPEND="
	dev-lisp/proc-parse
	dev-lisp/cl-ppcre
	dev-lisp/quri
	dev-lisp/local-time
	dev-lisp/quri
"
