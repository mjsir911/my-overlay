# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Simple Common Lisp XML Parser"
HOMEPAGE="http://www.common-lisp.net/project/s-xml/"
SRC_URI="http://common-lisp.net/~sionescu/files/${P}.tar.bz2"

LICENSE="LLGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="!dev-lisp/cl-${PN}"
RDEPEND="${DEPEND}"
BDEPEND=""

HTML_DOCS="doc/*.html doc/*.css"
