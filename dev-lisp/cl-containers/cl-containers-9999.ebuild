# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Containers Library for Common Lisp"
HOMEPAGE="http://common-lisp.net/project/cl-containers/"
EGIT_REPO_URI="https://github.com/gwkkwg/${PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

RDEPEND="=dev-lisp/metatilities-base-9999"
