# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Containers Library for Common Lisp"
HOMEPAGE="http://common-lisp.net/project/cl-containers/"
SRC_URI="https://github.com/gwkkwg/${PN}/archive/version-${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="=dev-lisp/metatilities-base-0.6.6"

S=${WORKDIR}/${PN}-version-${PV}
