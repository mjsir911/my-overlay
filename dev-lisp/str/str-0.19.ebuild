# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Modern, simple and consistent Common Lisp string manipulation library."
HOMEPAGE="https://vindarel.github.io/cl-str/"
SRC_URI="https://github.com/vindarel/cl-${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-lisp/cl-ppcre dev-lisp/cl-ppcre-unicode dev-lisp/cl-change-case"
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}/cl-${P}

src_install() {
	common-lisp-3_src_install
	common-lisp-install-sources -t all README.md # why
}
