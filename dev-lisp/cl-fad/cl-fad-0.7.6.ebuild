# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Portable pathname library for Common Lisp"
HOMEPAGE="https://edicl.github.io/cl-fad/"
SRC_URI="https://github.com/edicl/cl-fad/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="sbcl"

DEPEND="
sbcl? ( dev-lisp/sb-posix )
dev-lisp/bordeaux-threads dev-lisp/alexandria
"
RDEPEND="${DEPEND}"
BDEPEND=""

HTML_DOCS="docs/index.html"

src_install() {
	common-lisp-3_src_install
	einstalldocs
}
