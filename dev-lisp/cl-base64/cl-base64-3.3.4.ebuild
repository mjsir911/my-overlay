# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Base64 encoding and decoding with URI support"
HOMEPAGE="https://www.cliki.net/cl-base64"
SRC_URI="http://files.kpe.io/${PN}/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="test"

DEPEND="test? ( dev-lisp/ptester dev-lisp/kmrcl )"
RDEPEND="${DEPEND}"
BDEPEND=""
