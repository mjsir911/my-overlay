# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Common Lisp binding for SQLite"
HOMEPAGE="http://common-lisp.net/project/cl-sqlite/"
SRC_URI="https://github.com/dmitryvk/cl-sqlite/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE=test

CLSYSTEMS=sqlite

DEPEND="
	dev-db/sqlite:3
	dev-lisp/iterate
	dev-lisp/cffi
"
RDEPEND="${DEPEND}"
BDEPEND=""

HTML_DOCS="index.html style.css"

src_install() {
	if use test; then
		CLSYSTEMS="${CLSYSTEMS} sqlite-tests"
	fi
	common-lisp-3_src_install
	einstalldocs
}
