# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="fare-quasiquote implements a portable quasiquote that you can control."
HOMEPAGE="https://gitlab.common-lisp.net/frideau/fare-quasiquote"
EGIT_REPO_URI="https://gitlab.common-lisp.net/frideau/${PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-lisp/fare-utils"
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	common-lisp-3_src_install
	common-lisp-install-sources -t all version.text
}
