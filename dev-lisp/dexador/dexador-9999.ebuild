# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="A fast HTTP client for Common Lisp"
HOMEPAGE="https://github.com/fukamachi/dexador"
EGIT_REPO_URI="https://github.com/fukamachi/${PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

IUSE="+ssl"

RDEPEND="
	dev-lisp/fast-http
	dev-lisp/quri
	dev-lisp/fast-io
	dev-lisp/babel
	dev-lisp/trivial-gray-streams
	dev-lisp/chunga
	dev-lisp/cl-ppcre
	dev-lisp/cl-cookie
	dev-lisp/trivial-mimes
	dev-lisp/chipz
	dev-lisp/cl-base64
	dev-lisp/cl-reexport
	dev-lisp/usocket
	ssl? ( dev-lisp/cl+ssl )
	dev-lisp/bordeaux-threads
	dev-lisp/alexandria
"
