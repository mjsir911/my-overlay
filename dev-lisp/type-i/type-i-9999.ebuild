# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Type Inference Utility on unary type-checking predicates"
HOMEPAGE="https://github.com/guicho271828/type-i"
EGIT_REPO_URI="https://github.com/guicho271828/${PN}.git"

LICENSE="LLGPL"
SLOT="0"
KEYWORDS=""

RDEPEND="
	dev-lisp/introspect-environment
	dev-lisp/alexandria
	dev-lisp/trivia
	dev-lisp/lisp-namespace
"
