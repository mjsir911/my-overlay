# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="cl-cffi-gtk is a Lisp binding to the GTK+ 3 library. "
HOMEPAGE="http://www.crategus.com/books/cl-cffi-gtk"
# quicklisp points :
EGIT_REPO_URI="https://github.com/Ferada/cl-cffi-gtk.git"
# Upstream is:
# EGIT_REPO_URI="https://github.com/crategus/cl-cffi-gtk.git"
# Doesn't work with cl-webkit2 it looks like, gerror.lisp errors out

LICENSE="LGPL"
SLOT="0"
KEYWORDS=""

RDEPEND="
	dev-lisp/cffi
	dev-lisp/bordeaux-threads
	dev-lisp/iterate
	dev-lisp/trivial-features
	dev-lisp/split-sequence

	dev-lisp/trivial-garbage
	dev-lisp/closer-mop

	x11-libs/gtk+:3
	sys-libs/glibc
	x11-libs/gdk-pixbuf
	x11-libs/cairo
	>=x11-libs/pango-1.44
"

# PATCHES=(
# 	${FILESDIR}/${P}-sigfix.patch
# )
