# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Stat a file's size in Common Lisp."
HOMEPAGE="https://github.com/ruricolist/trivial-file-size"
EGIT_REPO_URI="https://github.com/ruricolist/${PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

IUSE="test sbcl allegro cmucl clisp"

DEPEND="
test? ( dev-lisp/fiveam )
sbcl? ( dev-lisp/sb-posix )
allegro? ( dev-lisp/osi )
cmucl? ( dev-lisp/unix )
clisp? ( dev-lisp/syscalls )
"
RDEPEND="${DEPEND}"
BDEPEND=""
