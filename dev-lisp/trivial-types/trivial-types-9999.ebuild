# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Trivial type definitions for Common Lisp"
HOMEPAGE="https://github.com/digikar99/trivial-types"
EGIT_REPO_URI="https://github.com/digikar99/${PN}.git"

LICENSE="LLGPL"
SLOT="0"
KEYWORDS=""

DEPEND="dev-lisp/fiveam"
RDEPEND="${DEPEND}"
BDEPEND=""
