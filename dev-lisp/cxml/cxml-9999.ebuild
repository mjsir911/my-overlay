
# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Closure XML - A Common Lisp XML Parser"
HOMEPAGE="http://common-lisp.net/project/cxml/"
EGIT_REPO_URI="git://repo.or.cz/${PN}.git"

LICENSE="LLGPL-2.1"
SLOT="0"
KEYWORDS=""

IUSE="scl doc"

# I can't find a version of asdf that this .asd file workes with where
# make-pathname returns a string (or something?)
DEPEND="
	!dev-lisp/cl-${PN}
	>=dev-lisp/closure-common-${PV}
	dev-lisp/puri
	scl? ( dev-lisp/trivial-gray-streams )
"
RDEPEND="${DEPEND}"
BDEPEND=""

HTML_DOCS="doc/*.html doc/*.css doc/*.png"
DOCS="README OLDNEWS TIMES"

src_compile() {
	common-lisp-3_src_compile
	emake -C doc
}

src_install() {
	common-lisp-3_src_install
	common-lisp-install-sources -t all catalog.dtd
}
