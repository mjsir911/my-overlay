# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

MY_PV=${PV:0:4}-${PV:4:2}-${PV:6:2}

DESCRIPTION="Closure XML - A Common Lisp XML Parser"
HOMEPAGE="http://common-lisp.net/project/cxml/"

#SRC_URI="https://common-lisp.net/project/cxml/download/cxml-2008-11-30.tgz"
SRC_URI="http://common-lisp.net/project/${PN}/download/${PN}-${MY_PV}.tgz"
# SRC_URI="https://github.com/sharplispers/cxml/archive/refs/heads/master.tar.gz -> ${P}.tar.gz"

LICENSE="LLGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE=scl

# I can't find a version of asdf that this .asd file workes with where
# make-pathname returns a string (or something?)
DEPEND="
	!dev-lisp/cl-${PN}
	>=dev-lisp/closure-common-${PV}
	dev-lisp/puri
	scl? ( dev-lisp/trivial-gray-streams )
"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}"/${PN}-${MY_PV}

HTML_DOCS="doc/*.html doc/*.css doc/*.png"
DOCS="README OLDNEWS TIMES"

src_install() {
	common-lisp-3_src_install
	common-lisp-install-sources -t all catalog.dtd
}
