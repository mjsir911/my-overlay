# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="
Parallel programming library providing the futures/promises synchronization mechanism"
HOMEPAGE="https://common-lisp.net/project/eager-future/"
SRC_URI="https://common-lisp.net/project/${PN}/release/${P}.tgz"

LICENSE="LGPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="dev-lisp/bordeaux-threads dev-lisp/trivial-garbage"
