# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Parallelism for Common Lisp"
HOMEPAGE="http://lparallel.org"
SRC_URI="https://github.com/lmj/lparallel/archive/refs/tags/lparallel-2.8.4.tar.gz -> ${P}.tar.gz"

LICENSE="BSD-3-Clause"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-lisp/alexandria dev-lisp/bordeaux-threads"
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}/${PN}-lparallel-2.8.4
