# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="A tiny collection of utilities to work with arrays and vectors."
HOMEPAGE="https://shinmera.github.io/array-utils"
EGIT_REPO_URI="https://github.com/Shinmera/${PN}.git"

LICENSE="ZLIB"
SLOT="0"
KEYWORDS=""

RDEPEND=""
