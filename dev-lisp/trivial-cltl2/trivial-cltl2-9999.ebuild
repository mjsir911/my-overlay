# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Portable CLtL2"
HOMEPAGE="https://github.com/Zulu-Inuoe/trivial-cltl2"
EGIT_REPO_URI="https://github.com/Zulu-Inuoe/${PN}.git"

LICENSE="LLGPL"
SLOT="0"
KEYWORDS=""

RDEPEND=""
