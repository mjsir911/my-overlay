# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="A Common Lisp abstraction layer over platform dependent functionality."
HOMEPAGE="https://github.com/svenvc/s-sysdeps"
SRC_URI="https://github.com/svenvc/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LLGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="!dev-lisp/cl-${PN}"
RDEPEND="${DEPEND}"
BDEPEND=""
