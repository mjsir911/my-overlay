# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Common Lisp regression testing framework"
HOMEPAGE="http://common-lisp.net/project/fiveam/"
SRC_URI="https://github.com/lispci/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="
	!dev-lisp/cl-${PN}
	!dev-lisp/cl-${PN}-darcs
	dev-lisp/alexandria
	dev-lisp/asdf-flv
	dev-lisp/trivial-backtrace
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	common-lisp-3_src_install
	common-lisp-install-sources -t all version.sexp
	einstalldocs
}
