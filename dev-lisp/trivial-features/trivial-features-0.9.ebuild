# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Ensures consistent *FEATURES* across multiple Common Lisp implementations."
HOMEPAGE="http://cliki.net/trivial-features"
SRC_URI="https://github.com/${PN}/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="test"

DEPEND="test? ( dev-lisp/rt dev-lisp/cffi dev-lisp/alexandria )"
RDEPEND="${DEPEND}"
BDEPEND=""

DOCS="README.md SPEC.md"

CLSYSTEMS="trivial-features"

src_install() {
	if use test; then
		CLSYSTEMS="${CLSYSTEMS} ${PN}-tests"
	fi
	common-lisp-3_src_install
}
