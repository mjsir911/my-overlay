# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Portable shared-state concurrency for Common Lisp"
HOMEPAGE="http://common-lisp.net/project/bordeaux-threads/"
SRC_URI="https://github.com/sionescu/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

DEPEND="dev-lisp/alexandria test? ( dev-lisp/fiveam ) "
RDEPEND="${DEPEND}"
BDEPEND=""

DOCS="CONTRIBUTORS README"

src_install() {
	common-lisp-install-sources -t all version.sexp
	common-lisp-3_src_install
}
