# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="FSet, the functional collections library for Common Lisp."
HOMEPAGE="https://common-lisp.net/project/fset/Site/index.html"
EGIT_REPO_URI="https://github.com/slburson/${PN}.git"

LICENSE="LLGPL"
SLOT="0"
KEYWORDS=""

RDEPEND="dev-lisp/misc-extensions dev-lisp/mt19937 dev-lisp/named-readtables"

src_install() {
	common-lisp-3_src_install
	common-lisp-install-asdf fset.asd
}
