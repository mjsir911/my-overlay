# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Sundry utilities for J.P. Larocque."
HOMEPAGE="https://www.thoughtcrime.us/software/cl-jpl-util/"
SRC_URI="https://www.thoughtcrime.us/software/cl-${PN}/cl-${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="dev-lisp/rsm"

S="${WORKDIR}/cl-${P}"
