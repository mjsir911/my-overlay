# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Common Lisp bindings for Enchant spell-checker library"
HOMEPAGE="https://github.com/tlikonen/cl-enchant"
EGIT_REPO_URI="https://github.com/tlikonen/cl-${PN}.git"

LICENSE="CC0-1.0"
SLOT="0"
KEYWORDS=""

RDEPEND="dev-lisp/cffi"
