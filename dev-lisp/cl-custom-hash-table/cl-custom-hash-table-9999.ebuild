# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Custom hash tables for Common Lisp"
HOMEPAGE="https://github.com/metawilm/cl-custom-hash-table"
EGIT_REPO_URI="https://github.com/metawilm/${PN}.git"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS=""

RDEPEND=""
