# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Calculate various string metrics efficiently in Common Lisp (Damerau-Levenshtein, Hamming, Jaro, Jaro-Winkler, Levenshtein, etc.)"
HOMEPAGE="https://github.com/cbaggers/mk-string-metrics"
SRC_URI="https://github.com/cbaggers/mk-string-metrics/archive/refs/tags/0.1.2.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}/${PN}-0.1.2
