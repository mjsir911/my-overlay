# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="These are metabang.com's Common Lisp basic utilities."
HOMEPAGE="http://common-lisp.net/project/metatilities-base/"
SRC_URI="https://github.com/gwkkwg/${PN}/archive/version-${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND=""

S=${WORKDIR}/${PN}-version-${PV}
