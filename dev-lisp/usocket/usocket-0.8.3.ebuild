# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Universal socket library for Common Lisp"
HOMEPAGE="https://common-lisp.net/project/usocket/"
SRC_URI="https://github.com/${PN}/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="test"

DEPEND="
!dev-lisp/cl-${PN}
dev-lisp/split-sequence dev-lisp/iolib
test? ( dev-lisp/rt dev-lisp/bordeaux-threads )
"
RDEPEND="${DEPEND}"
BDEPEND=""

DOCS="TODO README.md notes/"

CLSYSTEMS="${PN} ${PN}-server"

src_install() {
	if use test; then
		CLSYSTEMS="${CLSYSTEMS} ${PN}-test"
	fi
	common-lisp-3_src_install
	common-lisp-install-sources -t all version.sexp
}
