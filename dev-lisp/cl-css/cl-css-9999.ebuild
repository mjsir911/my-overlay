# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Non-validating, inline CSS generator for Common Lisp"
HOMEPAGE="https://github.com/inaimathi/cl-css"
EGIT_REPO_URI="https://github.com/inaimathi/${PN}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
