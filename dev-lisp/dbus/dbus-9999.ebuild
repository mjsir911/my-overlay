# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="A D-BUS client library for Common Lisp"
HOMEPAGE="https://github.com/death/dbus"
EGIT_REPO_URI="https://github.com/death/dbus.git"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS=""

RDEPEND="
	dev-lisp/alexandria
	dev-lisp/babel
	dev-lisp/cl-xmlspam
	dev-lisp/flexi-streams
	dev-lisp/iolib
	dev-lisp/ironclad
	dev-lisp/split-sequence
	dev-lisp/trivial-garbage
	dev-lisp/ieee-floats
"
