# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="A Lisp library for computing differences between sequences."
HOMEPAGE="https://github.com/wiseman/cl-difflib"
EGIT_REPO_URI="https://github.com/wiseman/cl-difflib.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

RDEPEND=""
