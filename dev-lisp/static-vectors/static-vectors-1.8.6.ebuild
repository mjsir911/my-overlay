# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Allocate SIMPLE-ARRAYs in static memory"
HOMEPAGE="http://common-lisp.net/projects/iolib/"
SRC_URI="https://github.com/sionescu/static-vectors/archive/refs/tags/v1.8.6.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-lisp/alexandria dev-lisp/cffi"
RDEPEND="${DEPEND}"
BDEPEND="dev-lisp/cffi"

S=${WORKDIR}/${PN}-1.8.6

src_install() {
	common-lisp-3_src_install
	common-lisp-install-sources -t all version.sexp
}
