# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="?"
HOMEPAGE="https://www.cliki.net/Misc-Extensions"
EGIT_REPO_URI="https://gitlab.common-lisp.net/misc-extensions/devel.git"

LICENSE="public-domain"
SLOT="0"
KEYWORDS=""

RDEPEND=""
