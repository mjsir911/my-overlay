# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Library providing a protocol for text-editor buffers."
HOMEPAGE="https://github.com/robert-strandh/Cluffer"
EGIT_REPO_URI="https://github.com/robert-strandh/Cluffer"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS=""

RDEPEND="dev-lisp/acclimation dev-lisp/clump"
