# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="A Common Lisp library that provides a namespace for readtables."
HOMEPAGE="http://melisgl.github.io/named-readtables"
SRC_URI="https://github.com/melisgl/named-readtables/archive/refs/tags/0.9.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

HTMLDOCS="doc/${PN}.html"
