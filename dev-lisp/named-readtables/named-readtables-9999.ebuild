# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="A Common Lisp library that provides a namespace for readtables."
HOMEPAGE="http://melisgl.github.io/named-readtables"
EGIT_REPO_URI="https://github.com/melisgl/${PN}.git"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
