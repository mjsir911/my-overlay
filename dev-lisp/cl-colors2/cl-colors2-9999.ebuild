# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Simple color library for Common Lisp"
HOMEPAGE="https://notabug.org/cage/cl-colors2"
EGIT_REPO_URI="https://notabug.org/cage/cl-colors2.git"

LICENSE="Boost-1.0"
SLOT="0"
KEYWORDS=""

RDEPEND="
	dev-lisp/alexandria
	dev-lisp/cl-ppcre
"
