# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Portable chunked streams for Common Lisp"
HOMEPAGE="https://edicl.github.io/chunga/"
SRC_URI="https://github.com/edicl/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-lisp/trivial-gray-streams"
RDEPEND="${DEPEND}"
BDEPEND=""

HTMLDOCS="docs/index.html"
