# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3 git-r3

DESCRIPTION="Basic functions and macros, interfaces, pure and stateful datastructures"
HOMEPAGE="https://gitlab.common-lisp.net/frideau/fare-utils"
HOMEPAGE="http://www.cliki.net/fare-utils"
EGIT_REPO_URI="https://gitlab.common-lisp.net/frideau/fare-utils.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=">=dev-lisp/asdf-3:="
RDEPEND="${DEPEND}"
BDEPEND=""
