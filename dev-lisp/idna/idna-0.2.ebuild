# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit common-lisp-3

DESCRIPTION="Common Lisp IDNA encding / decoding functions"
HOMEPAGE="https://github.com/antifuchs/idna"
SRC_URI="https://github.com/antifuchs/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-lisp/split-sequence"
RDEPEND="${DEPEND}"
BDEPEND=""
