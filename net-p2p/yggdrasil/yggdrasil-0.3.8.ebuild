# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit golang-build golang-vcs-snapshot

EGO_PN="github.com/yggdrasil-network/yggdrasil-go"
EGIT_COMMIT="v${PV}"
GIT_COMMIT="562a7d1f19f254e9d078aef44a78f7d1398bc4e5"
ARCHIVE_URI="https://${EGO_PN}/archive/${EGIT_COMMIT}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64"

DESCRIPTION="An experiment in scalable routing as an encrypted IPv6 overlay network"
HOMEPAGE="https://yggdrasil-network.github.io/"
SRC_URI="${ARCHIVE_URI}"
LICENSE="LGPLv3"

IUSE="hardened"
SLOT="0"

RESTRICT="test"
