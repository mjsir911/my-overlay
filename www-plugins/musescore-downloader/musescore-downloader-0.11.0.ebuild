# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit npm

DESCRIPTION="Download sheet music from musescore.com for free"
HOMEPAGE="https://github.com/Xmader/musescore-downloader"
# got this list with:
# cat package-lock.json  | grep resolved | rev | cut -d ' ' -f 1 | rev | tr -d '",'
read -r -d '' SRC_URI << EOF
	https://github.com/Xmader/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz
	https://registry.npmjs.org/@rollup/plugin-json/-/plugin-json-4.0.0.tgz
	https://registry.npmjs.org/@types/estree/-/estree-0.0.39.tgz
	https://registry.npmjs.org/@types/file-saver/-/file-saver-2.0.1.tgz
	https://registry.npmjs.org/@types/node/-/node-12.12.5.tgz
	https://registry.npmjs.org/@types/pdfkit/-/pdfkit-0.10.4.tgz
	https://registry.npmjs.org/@types/resolve/-/resolve-0.0.8.tgz
	https://registry.npmjs.org/abstract-leveldown/-/abstract-leveldown-0.12.4.tgz
	https://registry.npmjs.org/xtend/-/xtend-3.0.0.tgz
	https://registry.npmjs.org/acorn/-/acorn-7.4.1.tgz
	https://registry.npmjs.org/acorn-node/-/acorn-node-1.8.2.tgz
	https://registry.npmjs.org/acorn-walk/-/acorn-walk-7.0.0.tgz
	https://registry.npmjs.org/amdefine/-/amdefine-1.0.1.tgz
	https://registry.npmjs.org/array-from/-/array-from-2.1.1.tgz
	https://registry.npmjs.org/asn1.js/-/asn1.js-4.10.1.tgz
	https://registry.npmjs.org/ast-transform/-/ast-transform-0.0.0.tgz
	https://registry.npmjs.org/escodegen/-/escodegen-1.2.0.tgz
	https://registry.npmjs.org/esprima/-/esprima-1.0.4.tgz
	https://registry.npmjs.org/estraverse/-/estraverse-1.5.1.tgz
	https://registry.npmjs.org/esutils/-/esutils-1.0.0.tgz
	https://registry.npmjs.org/source-map/-/source-map-0.1.43.tgz
	https://registry.npmjs.org/ast-types/-/ast-types-0.7.8.tgz
	https://registry.npmjs.org/babel-runtime/-/babel-runtime-6.26.0.tgz
	https://registry.npmjs.org/base64-js/-/base64-js-1.3.1.tgz
	https://registry.npmjs.org/bl/-/bl-0.8.2.tgz
	https://registry.npmjs.org/isarray/-/isarray-0.0.1.tgz
	https://registry.npmjs.org/readable-stream/-/readable-stream-1.0.34.tgz
	https://registry.npmjs.org/string_decoder/-/string_decoder-0.10.31.tgz
	https://registry.npmjs.org/bn.js/-/bn.js-4.11.8.tgz
	https://registry.npmjs.org/brfs/-/brfs-2.0.2.tgz
	https://registry.npmjs.org/brorand/-/brorand-1.1.0.tgz
	https://registry.npmjs.org/brotli/-/brotli-1.3.2.tgz
	https://registry.npmjs.org/browser-resolve/-/browser-resolve-1.11.3.tgz
	https://registry.npmjs.org/resolve/-/resolve-1.1.7.tgz
	https://registry.npmjs.org/browserify-aes/-/browserify-aes-1.2.0.tgz
	https://registry.npmjs.org/browserify-cipher/-/browserify-cipher-1.0.1.tgz
	https://registry.npmjs.org/browserify-des/-/browserify-des-1.0.2.tgz
	https://registry.npmjs.org/browserify-fs/-/browserify-fs-1.0.0.tgz
	https://registry.npmjs.org/browserify-optional/-/browserify-optional-1.0.1.tgz
	https://registry.npmjs.org/browserify-rsa/-/browserify-rsa-4.0.1.tgz
	https://registry.npmjs.org/browserify-sign/-/browserify-sign-4.0.4.tgz
	https://registry.npmjs.org/buffer-equal/-/buffer-equal-0.0.1.tgz
	https://registry.npmjs.org/buffer-es6/-/buffer-es6-4.9.3.tgz
	https://registry.npmjs.org/buffer-from/-/buffer-from-1.1.1.tgz
	https://registry.npmjs.org/buffer-xor/-/buffer-xor-1.0.3.tgz
	https://registry.npmjs.org/builtin-modules/-/builtin-modules-3.1.0.tgz
	https://registry.npmjs.org/cipher-base/-/cipher-base-1.0.4.tgz
	https://registry.npmjs.org/clone/-/clone-1.0.4.tgz
	https://registry.npmjs.org/concat-stream/-/concat-stream-1.6.2.tgz
	https://registry.npmjs.org/convert-source-map/-/convert-source-map-1.7.0.tgz
	https://registry.npmjs.org/core-js/-/core-js-2.6.11.tgz
	https://registry.npmjs.org/core-util-is/-/core-util-is-1.0.2.tgz
	https://registry.npmjs.org/create-ecdh/-/create-ecdh-4.0.3.tgz
	https://registry.npmjs.org/create-hash/-/create-hash-1.2.0.tgz
	https://registry.npmjs.org/create-hmac/-/create-hmac-1.1.7.tgz
	https://registry.npmjs.org/crypto-browserify/-/crypto-browserify-3.12.0.tgz
	https://registry.npmjs.org/crypto-js/-/crypto-js-3.1.9-1.tgz
	https://registry.npmjs.org/d/-/d-1.0.1.tgz
	https://registry.npmjs.org/deep-equal/-/deep-equal-1.1.1.tgz
	https://registry.npmjs.org/deep-is/-/deep-is-0.1.3.tgz
	https://registry.npmjs.org/deferred-leveldown/-/deferred-leveldown-0.2.0.tgz
	https://registry.npmjs.org/define-properties/-/define-properties-1.1.3.tgz
	https://registry.npmjs.org/des.js/-/des.js-1.0.1.tgz
	https://registry.npmjs.org/dfa/-/dfa-1.2.0.tgz
	https://registry.npmjs.org/diffie-hellman/-/diffie-hellman-5.0.3.tgz
	https://registry.npmjs.org/duplexer2/-/duplexer2-0.1.4.tgz
	https://registry.npmjs.org/elliptic/-/elliptic-6.5.3.tgz
	https://registry.npmjs.org/errno/-/errno-0.1.7.tgz
	https://registry.npmjs.org/es-abstract/-/es-abstract-1.18.0-next.1.tgz
	https://registry.npmjs.org/object-inspect/-/object-inspect-1.8.0.tgz
	https://registry.npmjs.org/es-to-primitive/-/es-to-primitive-1.2.1.tgz
	https://registry.npmjs.org/es5-ext/-/es5-ext-0.10.53.tgz
	https://registry.npmjs.org/es6-iterator/-/es6-iterator-2.0.3.tgz
	https://registry.npmjs.org/es6-map/-/es6-map-0.1.5.tgz
	https://registry.npmjs.org/es6-set/-/es6-set-0.1.5.tgz
	https://registry.npmjs.org/es6-symbol/-/es6-symbol-3.1.1.tgz
	https://registry.npmjs.org/es6-symbol/-/es6-symbol-3.1.3.tgz
	https://registry.npmjs.org/escodegen/-/escodegen-1.9.1.tgz
	https://registry.npmjs.org/esprima/-/esprima-3.1.3.tgz
	https://registry.npmjs.org/estraverse/-/estraverse-4.3.0.tgz
	https://registry.npmjs.org/estree-is-function/-/estree-is-function-1.0.0.tgz
	https://registry.npmjs.org/estree-walker/-/estree-walker-0.6.1.tgz
	https://registry.npmjs.org/esutils/-/esutils-2.0.3.tgz
	https://registry.npmjs.org/event-emitter/-/event-emitter-0.3.5.tgz
	https://registry.npmjs.org/evp_bytestokey/-/evp_bytestokey-1.0.3.tgz
	https://registry.npmjs.org/ext/-/ext-1.4.0.tgz
	https://registry.npmjs.org/type/-/type-2.0.0.tgz
	https://registry.npmjs.org/fast-levenshtein/-/fast-levenshtein-2.0.6.tgz
	https://registry.npmjs.org/file-saver/-/file-saver-2.0.2.tgz
	https://registry.npmjs.org/fontkit/-/fontkit-1.8.1.tgz
	https://registry.npmjs.org/foreach/-/foreach-2.0.5.tgz
	https://registry.npmjs.org/function-bind/-/function-bind-1.1.1.tgz
	https://registry.npmjs.org/fwd-stream/-/fwd-stream-1.0.4.tgz
	https://registry.npmjs.org/isarray/-/isarray-0.0.1.tgz
	https://registry.npmjs.org/readable-stream/-/readable-stream-1.0.34.tgz
	https://registry.npmjs.org/string_decoder/-/string_decoder-0.10.31.tgz
	https://registry.npmjs.org/get-assigned-identifiers/-/get-assigned-identifiers-1.2.0.tgz
	https://registry.npmjs.org/has/-/has-1.0.3.tgz
	https://registry.npmjs.org/has-symbols/-/has-symbols-1.0.1.tgz
	https://registry.npmjs.org/hash-base/-/hash-base-3.0.4.tgz
	https://registry.npmjs.org/hash.js/-/hash.js-1.1.7.tgz
	https://registry.npmjs.org/hmac-drbg/-/hmac-drbg-1.0.1.tgz
	https://registry.npmjs.org/idb-wrapper/-/idb-wrapper-1.7.2.tgz
	https://registry.npmjs.org/indexof/-/indexof-0.0.1.tgz
	https://registry.npmjs.org/inherits/-/inherits-2.0.4.tgz
	https://registry.npmjs.org/is/-/is-0.2.7.tgz
	https://registry.npmjs.org/is-arguments/-/is-arguments-1.0.4.tgz
	https://registry.npmjs.org/is-callable/-/is-callable-1.2.2.tgz
	https://registry.npmjs.org/is-date-object/-/is-date-object-1.0.2.tgz
	https://registry.npmjs.org/is-module/-/is-module-1.0.0.tgz
	https://registry.npmjs.org/is-negative-zero/-/is-negative-zero-2.0.0.tgz
	https://registry.npmjs.org/is-object/-/is-object-0.1.2.tgz
	https://registry.npmjs.org/is-reference/-/is-reference-1.1.4.tgz
	https://registry.npmjs.org/is-regex/-/is-regex-1.1.1.tgz
	https://registry.npmjs.org/is-symbol/-/is-symbol-1.0.3.tgz
	https://registry.npmjs.org/isarray/-/isarray-1.0.0.tgz
	https://registry.npmjs.org/isbuffer/-/isbuffer-0.0.0.tgz
	https://registry.npmjs.org/level-blobs/-/level-blobs-0.1.7.tgz
	https://registry.npmjs.org/isarray/-/isarray-0.0.1.tgz
	https://registry.npmjs.org/readable-stream/-/readable-stream-1.1.14.tgz
	https://registry.npmjs.org/string_decoder/-/string_decoder-0.10.31.tgz
	https://registry.npmjs.org/level-filesystem/-/level-filesystem-1.2.0.tgz
	https://registry.npmjs.org/xtend/-/xtend-2.2.0.tgz
	https://registry.npmjs.org/level-fix-range/-/level-fix-range-1.0.2.tgz
	https://registry.npmjs.org/level-hooks/-/level-hooks-4.5.0.tgz
	https://registry.npmjs.org/level-js/-/level-js-2.2.4.tgz
	https://registry.npmjs.org/object-keys/-/object-keys-0.4.0.tgz
	https://registry.npmjs.org/xtend/-/xtend-2.1.2.tgz
	https://registry.npmjs.org/level-peek/-/level-peek-1.0.6.tgz
	https://registry.npmjs.org/level-sublevel/-/level-sublevel-5.2.3.tgz
	https://registry.npmjs.org/clone/-/clone-0.1.19.tgz
	https://registry.npmjs.org/level-fix-range/-/level-fix-range-2.0.0.tgz
	https://registry.npmjs.org/object-keys/-/object-keys-0.2.0.tgz
	https://registry.npmjs.org/xtend/-/xtend-2.0.6.tgz
	https://registry.npmjs.org/levelup/-/levelup-0.18.6.tgz
	https://registry.npmjs.org/isarray/-/isarray-0.0.1.tgz
	https://registry.npmjs.org/prr/-/prr-0.0.0.tgz
	https://registry.npmjs.org/readable-stream/-/readable-stream-1.0.34.tgz
	https://registry.npmjs.org/string_decoder/-/string_decoder-0.10.31.tgz
	https://registry.npmjs.org/xtend/-/xtend-3.0.0.tgz
	https://registry.npmjs.org/levn/-/levn-0.3.0.tgz
	https://registry.npmjs.org/linebreak/-/linebreak-1.0.2.tgz
	https://registry.npmjs.org/base64-js/-/base64-js-0.0.8.tgz
	https://registry.npmjs.org/brfs/-/brfs-2.0.2.tgz
	https://registry.npmjs.org/static-module/-/static-module-3.0.3.tgz
	https://registry.npmjs.org/unicode-trie/-/unicode-trie-1.0.0.tgz
	https://registry.npmjs.org/ltgt/-/ltgt-2.2.1.tgz
	https://registry.npmjs.org/magic-string/-/magic-string-0.22.5.tgz
	https://registry.npmjs.org/md5.js/-/md5.js-1.3.5.tgz
	https://registry.npmjs.org/merge-source-map/-/merge-source-map-1.0.4.tgz
	https://registry.npmjs.org/source-map/-/source-map-0.5.7.tgz
	https://registry.npmjs.org/miller-rabin/-/miller-rabin-4.0.1.tgz
	https://registry.npmjs.org/minimalistic-assert/-/minimalistic-assert-1.0.1.tgz
	https://registry.npmjs.org/minimalistic-crypto-utils/-/minimalistic-crypto-utils-1.0.1.tgz
	https://registry.npmjs.org/minimist/-/minimist-1.2.5.tgz
	https://registry.npmjs.org/next-tick/-/next-tick-1.0.0.tgz
	https://registry.npmjs.org/object-inspect/-/object-inspect-1.4.1.tgz
	https://registry.npmjs.org/object-is/-/object-is-1.1.3.tgz
	https://registry.npmjs.org/object-keys/-/object-keys-1.1.1.tgz
	https://registry.npmjs.org/object.assign/-/object.assign-4.1.1.tgz
	https://registry.npmjs.org/octal/-/octal-1.0.0.tgz
	https://registry.npmjs.org/once/-/once-1.4.0.tgz
	https://registry.npmjs.org/optionator/-/optionator-0.8.3.tgz
	https://registry.npmjs.org/pako/-/pako-0.2.9.tgz
	https://registry.npmjs.org/parse-asn1/-/parse-asn1-5.1.5.tgz
	https://registry.npmjs.org/path-parse/-/path-parse-1.0.6.tgz
	https://registry.npmjs.org/pbkdf2/-/pbkdf2-3.0.17.tgz
	https://registry.npmjs.org/png-js/-/png-js-1.0.0.tgz
	https://registry.npmjs.org/prelude-ls/-/prelude-ls-1.1.2.tgz
	https://registry.npmjs.org/process-es6/-/process-es6-0.11.6.tgz
	https://registry.npmjs.org/process-nextick-args/-/process-nextick-args-2.0.1.tgz
	https://registry.npmjs.org/prr/-/prr-1.0.1.tgz
	https://registry.npmjs.org/public-encrypt/-/public-encrypt-4.0.3.tgz
	https://registry.npmjs.org/quote-stream/-/quote-stream-1.0.2.tgz
	https://registry.npmjs.org/randombytes/-/randombytes-2.1.0.tgz
	https://registry.npmjs.org/randomfill/-/randomfill-1.0.4.tgz
	https://registry.npmjs.org/readable-stream/-/readable-stream-2.3.6.tgz
	https://registry.npmjs.org/regenerator-runtime/-/regenerator-runtime-0.11.1.tgz
	https://registry.npmjs.org/regexp.prototype.flags/-/regexp.prototype.flags-1.3.0.tgz
	https://registry.npmjs.org/es-abstract/-/es-abstract-1.17.7.tgz
	https://registry.npmjs.org/object-inspect/-/object-inspect-1.8.0.tgz
	https://registry.npmjs.org/resolve/-/resolve-1.12.0.tgz
	https://registry.npmjs.org/restructure/-/restructure-0.5.4.tgz
	https://registry.npmjs.org/ripemd160/-/ripemd160-2.0.2.tgz
	https://registry.npmjs.org/rollup/-/rollup-1.26.3.tgz
	https://registry.npmjs.org/rollup-plugin-commonjs/-/rollup-plugin-commonjs-10.1.0.tgz
	https://registry.npmjs.org/magic-string/-/magic-string-0.25.4.tgz
	https://registry.npmjs.org/rollup-plugin-node-builtins/-/rollup-plugin-node-builtins-2.1.2.tgz
	https://registry.npmjs.org/rollup-plugin-node-globals/-/rollup-plugin-node-globals-1.4.0.tgz
	https://registry.npmjs.org/acorn/-/acorn-5.7.4.tgz
	https://registry.npmjs.org/estree-walker/-/estree-walker-0.5.2.tgz
	https://registry.npmjs.org/rollup-plugin-node-resolve/-/rollup-plugin-node-resolve-5.2.0.tgz
	https://registry.npmjs.org/rollup-plugin-typescript/-/rollup-plugin-typescript-1.0.1.tgz
	https://registry.npmjs.org/rollup-pluginutils/-/rollup-pluginutils-2.8.2.tgz
	https://registry.npmjs.org/safe-buffer/-/safe-buffer-5.1.2.tgz
	https://registry.npmjs.org/scope-analyzer/-/scope-analyzer-2.0.5.tgz
	https://registry.npmjs.org/semver/-/semver-2.3.2.tgz
	https://registry.npmjs.org/sha.js/-/sha.js-2.4.11.tgz
	https://registry.npmjs.org/shallow-copy/-/shallow-copy-0.0.1.tgz
	https://registry.npmjs.org/source-map/-/source-map-0.6.1.tgz
	https://registry.npmjs.org/sourcemap-codec/-/sourcemap-codec-1.4.6.tgz
	https://registry.npmjs.org/static-eval/-/static-eval-2.0.3.tgz
	https://registry.npmjs.org/escodegen/-/escodegen-1.12.0.tgz
	https://registry.npmjs.org/static-module/-/static-module-3.0.4.tgz
	https://registry.npmjs.org/escodegen/-/escodegen-1.14.3.tgz
	https://registry.npmjs.org/esprima/-/esprima-4.0.1.tgz
	https://registry.npmjs.org/magic-string/-/magic-string-0.25.1.tgz
	https://registry.npmjs.org/object-inspect/-/object-inspect-1.8.0.tgz
	https://registry.npmjs.org/static-eval/-/static-eval-2.1.0.tgz
	https://registry.npmjs.org/string-range/-/string-range-1.2.2.tgz
	https://registry.npmjs.org/string.prototype.trimend/-/string.prototype.trimend-1.0.2.tgz
	https://registry.npmjs.org/string.prototype.trimstart/-/string.prototype.trimstart-1.0.2.tgz
	https://registry.npmjs.org/string_decoder/-/string_decoder-1.1.1.tgz
	https://registry.npmjs.org/svg-to-pdfkit/-/svg-to-pdfkit-0.1.8.tgz
	https://registry.npmjs.org/pdfkit/-/pdfkit-0.11.0.tgz
	https://registry.npmjs.org/through/-/through-2.3.8.tgz
	https://registry.npmjs.org/through2/-/through2-2.0.5.tgz
	https://registry.npmjs.org/tiny-inflate/-/tiny-inflate-1.0.3.tgz
	https://registry.npmjs.org/tslib/-/tslib-1.10.0.tgz
	https://registry.npmjs.org/type/-/type-1.2.0.tgz
	https://registry.npmjs.org/type-check/-/type-check-0.3.2.tgz
	https://registry.npmjs.org/typedarray/-/typedarray-0.0.6.tgz
	https://registry.npmjs.org/typedarray-to-buffer/-/typedarray-to-buffer-1.0.4.tgz
	https://registry.npmjs.org/typescript/-/typescript-4.1.1-rc.tgz
	https://registry.npmjs.org/unicode-properties/-/unicode-properties-1.3.1.tgz
	https://registry.npmjs.org/unicode-trie/-/unicode-trie-2.0.0.tgz
	https://registry.npmjs.org/unicode-trie/-/unicode-trie-0.3.1.tgz
	https://registry.npmjs.org/util-deprecate/-/util-deprecate-1.0.2.tgz
	https://registry.npmjs.org/vlq/-/vlq-0.2.3.tgz
	https://registry.npmjs.org/webmscore/-/webmscore-0.10.4.tgz
	https://registry.npmjs.org/word-wrap/-/word-wrap-1.2.3.tgz
	https://registry.npmjs.org/wrappy/-/wrappy-1.0.2.tgz
	https://registry.npmjs.org/xtend/-/xtend-4.0.2.tgz
EOF

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""


DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="net-libs/nodejs[npm]"

PATCHES=(
	"${FILESDIR}/${PN}-use-official-pdfkit.patch"
)


src_unpack() {
	npm_src_unpack
}

src_compile() {
	npm_src_compile
	npm run build || die
	npm run pack:ext || die
}

src_install() {
	# See https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Distribution_options/Sideloading_add-ons#Installation_using_the_standard_extension_folders
	insinto "/usr/share/mozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/"
	# passff@invicem.pro is the extension id found in the manifest.json
	newins "dist/ext.zip" "{69856097-6e10-42e9-acc7-0c063550c7b8}.xpi"
}
