# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{7,8} )
DISTUTILS_SINGLE_IMPL=1
DISTUTILS_USE_SETUPTOOLS=rdepend

inherit distutils-r1
inherit git-r3

DESCRIPTION=" Another piece of your extended mind "
HOMEPAGE="https://beepb00p.xyz/promnesia.html"

EGIT_REPO_URI="https://github.com/karlicoss/${PN}.git"
EGIT_COMMIT="v${PV}"
EGIT_CLONE_TYPE="shallow"
EGIT_SUBMODULES=()
# SRC_URI="https://github.com/karlicoss/${PN}/archive/v${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

BDEPEND="
	$(python_gen_cond_dep '
		dev-python/setuptools_scm[${PYTHON_USEDEP}]
	')
"

RDEPEND="
	$(python_gen_cond_dep '
			dev-python/tzlocal[${PYTHON_USEDEP}]
			dev-python/hug[${PYTHON_USEDEP}]

			dev-python/appdirs[${PYTHON_USEDEP}]
			dev-python/urlextract[${PYTHON_USEDEP}]
			|| ( dev-python/python-magic[${PYTHON_USEDEP}] sys-apps/file[python,${PYTHON_USEDEP}] )

			dev-python/more-itertools[${PYTHON_USEDEP}]
			dev-python/pytz[${PYTHON_USEDEP}]
			dev-python/sqlalchemy[${PYTHON_USEDEP}]
			dev-python/cachew[${PYTHON_USEDEP}]
	')
"

DEPEND="
"
