# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="Raspberry Pi USB booting code"
HOMEPAGE="https://github.com/raspberrypi/usbboot"
SRC_URI=""
EGIT_REPO_URI="https://github.com/raspberrypi/usbboot.git"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="virtual/libusb:1"
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	dobin rpiboot
}
