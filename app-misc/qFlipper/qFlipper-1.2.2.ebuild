# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit qmake-utils

DESCRIPTION="qFlipper — desktop application for updating Flipper Zero firmware via PC"
HOMEPAGE="https://update.flipperzero.one/"
SRC_URI="https://github.com/flipperdevices/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz
         https://github.com/nanopb/nanopb/archive/13666952914f3cf43a70c6b9738a7dc0dd06a6dc.tar.gz -> nanopb-1366695291.tar.gz
"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="
	>=dev-qt/qtcore-5.15.0:5
	virtual/libusb
	dev-qt/qtserialport
	dev-qt/qtquickcontrols2
	>=sys-libs/zlib-1.2.0
"

src_unpack() {
	default
	mv -T nanopb* qFlipper*/3rdparty/nanopb
}
src_configure() {
	eqmake5 PREFIX=$PREFIX -spec linux-g++ CONFIG+=qtquickcompiler DEFINES+=DISABLE_APPLICATION_UPDATES
}

src_install() {
	emake INSTALL_ROOT=${D} install
}
