# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Logo implmentation"
HOMEPAGE="https://people.eecs.berkeley.edu/~bh/logo.html"
SRC_URI="ftp://ftp.cs.berkeley.edu/pub/${PN}/${PN}.tar.gz"

SLOT="0"
LICENSE="GPL"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="sys-libs/ncurses app-text/texi2html"
RDEPEND="${DEPEND}"
BDEPEND=""

PATCHES=(
	"${FILESDIR}/${P}-get-working.patch"
)

DOCS=("${S}/docs/usermanual.pdf")
HTML_DOCS=("${S}/docs/html/")

src_compile() {
	(cd docs && emake all)
	rm "${S}/logo"
	default
}

src_install() {
	dobin logo
	insinto /usr/lib/logo
	doins -r logolib/
	doins -r helpfiles/
	doins -r csls/
	doinfo docs/ucblogo.info
	einstalldocs
}
