# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

MY_PN="apl"
MY_P="${MY_PN}-${PV}"
DESCRIPTION="Interpreter for the programming language APL."
HOMEPAGE="https://www.gnu.org/software/apl/"
SRC_URI="ftp://gnu.mirror.iweb.com/${MY_PN}/${MY_P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

S="${WORKDIR}/apl-1.8"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
