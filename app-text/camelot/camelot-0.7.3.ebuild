# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python2_7 python3_{6,7,8,9} )
DISTUTILS_SINGLE_IMPL=1

inherit distutils-r1

DESCRIPTION="PDF Table Extraction for Humans"
HOMEPAGE="https://camelot-py.readthedocs.io"
SRC_URI="https://github.com/atlanhq/${PN}/archive/v${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

RDEPEND="
	$(python_gen_cond_dep '
		>=dev-python/click-6.7[${PYTHON_USEDEP}]
		dev-python/matplotlib[${PYTHON_USEDEP}]
		>=dev-python/numpy-1.13.3[${PYTHON_USEDEP}]
		media-libs/opencv[python,${PYTHON_USEDEP}]
		>=dev-python/openpyxl-2.5.8[${PYTHON_USEDEP}]
		>=dev-python/pandas-0.23.4[${PYTHON_USEDEP}]
		>=dev-python/PyPDF2-1.26.0[${PYTHON_USEDEP}]
		>=app-text/pdfminer-20170720[${PYTHON_USEDEP}]
	')
"

src_prepare() {
	find . -type f -print0 | xargs -0 sed -i 's/pdfminer.six/pdfminer/g'
	default
}
