# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CRATES="
proc-macro2-0.4.27
pkg-config-0.3.14
lazycell-1.2.1
winapi-0.2.8
fuchsia-zircon-0.3.3
libpulse-binding-2.6.0
clap-2.33.0
fuchsia-zircon-sys-0.3.3
winapi-x86_64-pc-windows-gnu-0.4.0
nix-0.13.0
strsim-0.8.0
failure_derive-0.1.5
unicode-xid-0.1.0
mio-0.6.16
ansi_term-0.11.0
winapi-i686-pc-windows-gnu-0.4.0
iovec-0.1.2
atty-0.2.11
redox_termios-0.1.1
ws2_32-sys-0.2.1
xidlehook-0.7.1
vec_map-0.8.1
rustc-demangle-0.1.13
redox_syscall-0.1.54
failure-0.1.5
autocfg-0.1.2
libc-0.2.51
xcb-0.8.2
termion-1.5.1
unicode-width-0.1.5
log-0.4.6
kernel32-sys-0.2.2
quote-0.6.12
bitflags-1.0.4
miow-0.2.1
net2-0.2.33
syn-0.15.30
winapi-0.3.7
backtrace-0.3.15
cfg-if-0.1.7
backtrace-sys-0.1.28
synstructure-0.10.1
void-1.0.2
cc-1.0.35
winapi-build-0.1.1
slab-0.4.2
libpulse-sys-1.5.0
textwrap-0.11.0
x11-2.18.1
"

inherit cargo

DESCRIPTION="xautolock rewrite in Rust, with a few extra features"
HOMEPAGE="https://gitlab.com/jD91mZM2/xidlehook"
SRC_URI="$(cargo_crate_uris ${CRATES})"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+pulseaudio"

DEPEND=""
RDEPEND="
	x11-libs/libXScrnSaver
	pulseaudio? ( media-sound/pulseaudio )
"
BDEPEND="${RDEPEND}
	>=virtual/rust-1.34
"

src_compile() {
	cargo_src_compile --no-default-features $(usex pulseaudio "--features pulse" "")
}
