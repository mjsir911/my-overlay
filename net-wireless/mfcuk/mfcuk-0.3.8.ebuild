# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

DESCRIPTION="MiFare Classic Universal toolKit"
HOMEPAGE="https://github.com/nfc-tools/mfcuk"
SRC_URI="https://github.com/nfc-tools/${PN}/archive/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-libs/libnfc"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/${PN}-${P}"

src_prepare() {
	default
	eautoreconf
}
