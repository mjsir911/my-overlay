# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

MY_PV=1b6d022668080aa761a4e2bea8c24c1f7aa537db
MY_P="${PN}-${MY_PV}"

DESCRIPTION="MiFare Classic Universal toolKit"
HOMEPAGE="https://github.com/nfc-tools/mfcuk"
SRC_URI="https://github.com/nfc-tools/${PN}/archive/${MY_PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="=dev-libs/libnfc-1.5.1"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/${MY_P}"

src_prepare() {
	default
	eautoreconf
}
