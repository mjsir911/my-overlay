# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{6,7,8} )
DISTUTILS_USE_SETUPTOOLS=rdepend
inherit distutils-r1

DESCRIPTION="Hug aims to make developing APIs as simple as possible, but no simpler. "
HOMEPAGE="https://github.com/hugapi/hug"
SRC_URI="https://github.com/hugapi/${PN}/archive/${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="
	=dev-python/falcon-2.0.0[${PYTHON_USEDEP}]
	dev-python/requests[${PYTHON_USEDEP}]
	${DEPEND}
"
BDEPEND="dev-python/pytest-runner[${PYTHON_USEDEP}]"
