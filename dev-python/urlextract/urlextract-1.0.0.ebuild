# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

MY_PN="URLExtract"
MY_P="${MY_PN}-${PV}"
PYTHON_COMPAT=( python2_7 python3_7 )
inherit distutils-r1

DESCRIPTION="
	URLExtract is python class for collecting (extracting) URLs from given text based on locating TLD.
"
HOMEPAGE="https://github.com/lipoja/URLExtract"
SRC_URI="https://github.com/lipoja/${MY_PN}/archive/v${PV}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	dev-python/idna[${PYTHON_USEDEP}]
	dev-python/uritools[${PYTHON_USEDEP}]
	dev-python/appdirs[${PYTHON_USEDEP}]
	dev-python/filelock[${PYTHON_USEDEP}]
"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/${MY_P}"
