# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{7,8} )
inherit distutils-r1 git-r3

DESCRIPTION="Transparent and persistent cache/serialization powered by type hints"
HOMEPAGE="https://beepb00p.xyz/unnecessary-db.html#cachew"
# SRC_URI="https://github.com/karlicoss/cachew/archive/v0.6.3.tar.gz"
EGIT_REPO_URI="https://github.com/karlicoss/${PN}.git"
EGIT_COMMIT="v${PV}"
EGIT_CLONE_TYPE="shallow"
# EGIT_SUBMODULES=()

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="
	dev-python/sqlalchemy[${PYTHON_USEDEP}]
	${DEPEND}
"
BDEPEND="
	dev-python/setuptools_scm[${PYTHON_USEDEP}]
"
