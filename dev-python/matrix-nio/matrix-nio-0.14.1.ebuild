# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python2_7 python3_7 )
inherit distutils-r1

DESCRIPTION="A Python Matrix client library, designed according to sans I/O  principles"
HOMEPAGE="https://github.com/poljar/matrix-nio"
SRC_URI="https://github.com/poljar/matrix-nio/archive/0.14.1.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="e2e test"
RESTRICT="!test? ( test )"

RDEPEND="
	dev-python/future[${PYTHON_USEDEP}]
	dev-python/aiohttp[${PYTHON_USEDEP}]
	dev-python/aiofiles[${PYTHON_USEDEP}]
	dev-python/h11[${PYTHON_USEDEP}]
	dev-python/hyper-h2[${PYTHON_USEDEP}]
	dev-python/logbook[${PYTHON_USEDEP}]
	dev-python/jsonschema[${PYTHON_USEDEP}]
	dev-python/python-unpaddedbase64[${PYTHON_USEDEP}]
	dev-python/pycryptodome[${PYTHON_USEDEP}]

	e2e? (
		>=dev-libs/olm-3.1.0[python,${PYTHON_USEDEP}]
		>=dev-python/peewee-3.9.5[${PYTHON_USEDEP}]
		dev-python/cachetools[${PYTHON_USEDEP}]
		dev-python/atomicwrites[${PYTHON_USEDEP}]
	)
"
# >=python/peewee-3.9.5[${PYTHON_USEDEP}]

DEPEND=""
BDEPEND="
	test? (
		${RDEPEND}
		dev-python/pytest
		dev-python/hypothesis[${PYTHON_USEDEP}]
		dev-python/faker[${PYTHON_USEDEP}]
	)
"

src_test() {
	pytest
}
