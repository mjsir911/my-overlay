# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

USE_PHP="php8-0"

inherit git-r3

DESCRIPTION="PHP bindings for libgit2"
HOMEPAGE="http://libgit2.github.com/"
EGIT_REPO_URI="https://github.com/mjsir911/${PN}"
SRC_URI="https://github.com/nikic/PHP-Parser/archive/v4.9.0.tar.gz -> PHP-Parser-4.9.0.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="test"
RESTRICT="!test? ( test )"

BDEPEND="dev-libs/libgit2"
RDEPEND="${DEPEND}"
BDEPEND=""



PHP_EXT_NAME=git2

inherit php-ext-source-r3

src_unpack() {
	git-r3_src_unpack
	default
}


src_prepare() {
	php-ext-source-r3_src_prepare
	for slot in $(php_get_slots); do
		cp --recursive --preserve ${WORKDIR}/PHP-Parser-4.9.0 ${WORKDIR}/${slot}/build
	done
}
