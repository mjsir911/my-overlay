# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit cmake-utils

DESCRIPTION="Snowman is a native code to C/C++ decompiler"
HOMEPAGE="https://derevenets.com/"
SRC_URI="https://github.com/yegord/${PN}/archive/v${PV}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	>=dev-libs/boost-1.49
	>=dev-qt/qtcore-4.7
	>=dev-util/cmake-3.7

	sys-libs/binutils-libs
"
RDEPEND="${DEPEND}"
BDEPEND=""

CMAKE_USE_DIR="${S}/src"
