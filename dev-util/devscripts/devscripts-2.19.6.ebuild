# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="scripts to make the life of a Debian Package maintainer easier"
HOMEPAGE="https://salsa.debian.org/debian/devscripts/"
SRC_URI="https://salsa.debian.org/debian/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""


DEPEND="app-arch/dpkg dev-libs/libxslt app-text/docbook2X"
RDEPEND=""
BDEPEND=""

PATCHES=( 
	"${FILESDIR}/${P}-standard-dirnames.patch"
	"${FILESDIR}/${P}-undo-xsltproc-fix.patch" 
	"${FILESDIR}/${P}-remove-install-layout.patch"
)

S=$WORKDIR/${PN}-v${PV}
