# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION=""
HOMEPAGE="https://developer.android.com/studio/#command-tools"
SRC_URI="https://dl.google.com/android/repository/sdk-tools-linux-${PV}.zip"

LICENSE="android"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND} !dev-util/android-sdk-update-manager dev-java/swt"
BDEPEND=""

S="${WORKDIR}/tools"

PATCHES=${FILESDIR}/

src_install() {
	default
	MY_EPREFIX=$_E_DESTTREE_

	insinto $MY_EPREFIX/$(get_libdir)/android-sdk/
	doins -r lib support

	exeinto $_E_INSDESTTREE_
	doexe android emulator emulator-check mksdcard monitor

	exeinto $_E_INSDESTTREE_/bin
	doexe bin/*

	insinto $MY_EPREFIX/$(get_libdir)/android-sdk
	exeinto $MY_EPREFIX/bin
	for binfile in bin/* android emulator emulator-check mksdcard monitor; do
		dosym ${D}$_E_INSDESTTREE_/$binfile /$_E_EXEDESTTREE_/$(basename $binfile)
	done
}
