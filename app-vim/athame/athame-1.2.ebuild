# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

#VIM_PLUGIN_VIM_VERSION="7.0"
inherit vim-plugin

DESCRIPTION="Full vim for your shell"
HOMEPAGE="https://github.com/ardagnir/athame"
SRC_URI="https://github.com/ardagnir/${PN}/archive/v${PV}.tar.gz"

LICENSE="GPL-3"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="app-editors/vim:=[-minimal] ${DEPEND}"
BDEPEND=""

