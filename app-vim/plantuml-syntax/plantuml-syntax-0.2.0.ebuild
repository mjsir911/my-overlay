# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit vim-plugin

DESCRIPTION="vim plugin: plantuml file syntax highlighting"
HOMEPAGE="https://github.com/aklt/plantuml-syntax"
SRC_URI="https://github.com/aklt/${PN}/archive/v${PV}.tar.gz"
LICENSE="MIT"
KEYWORDS="alpha amd64 ia64 ~mips ppc ppc64 sparc x86"
IUSE=""

VIM_PLUGIN_HELPTEXT="vim syntax file for plantuml "
