# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

#VIM_PLUGIN_VIM_VERSION="7.0"
inherit vim-plugin

MY_PN="nim.vim"
MY_PR=${PR//r/}
MY_PVR="${PV}@${MY_PR}"
MY_PF="${MY_PN}-${MY_PVR}"

DESCRIPTION="vim plugin: nim syntax highlighting"
HOMEPAGE="https://github.com/zah/nim.vim"
SRC_URI="https://github.com/zah/${MY_PN}/archive/${MY_PVR}.tar.gz"
LICENSE=""
KEYWORDS="~amd64 ~x86"
IUSE=""

S="${WORKDIR}/${MY_PF//@/-}"

VIM_PLUGIN_HELPFILES=""
VIM_PLUGIN_HELPTEXT=""
VIM_PLUGIN_HELPURI=""
VIM_PLUGIN_MESSAGES=""
