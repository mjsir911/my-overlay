# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit vim-plugin

DESCRIPTION="vim plugin: Julia file syntax highlighting"
HOMEPAGE="https://github.com/JuliaEditorSupport/julia-vim"
SRC_URI="https://github.com/JuliaEditorSupport/julia-vim/archive/v${PV}.tar.gz"
LICENSE="MIT"
KEYWORDS="alpha amd64 ia64 ~mips ppc ppc64 sparc x86"
IUSE=""

VIM_PLUGIN_HELPTEXT=\
"This plugin provides syntax highlighting for Julia files."
