# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

#VIM_PLUGIN_VIM_VERSION="7.0"
inherit vim-plugin

MY_PN="logo.vim"

DESCRIPTION="vim plugin: logo syntax highlighting"
HOMEPAGE="https://github.com/lkdjiin/logo.vim"
SRC_URI="https://github.com/lkdjiin/${MY_PN}/archive/v${PV}.tar.gz"
LICENSE=""
KEYWORDS="~amd64 ~x86"
IUSE=""

S="${WORKDIR}/${MY_PN}-${PV}"

VIM_PLUGIN_HELPFILES=""
VIM_PLUGIN_HELPTEXT=""
VIM_PLUGIN_HELPURI=""
VIM_PLUGIN_MESSAGES=""

src_compile() {
	mkdir "${S}/syntax"
	mv "${S}/${MY_PN}" "${S}/syntax"
}
