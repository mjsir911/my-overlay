# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
PYTHON_COMPAT=( python3_{6,7} )

inherit python-single-r1

DESCRIPTION="Weechat Matrix protocol script written in python"
HOMEPAGE="https://github.com/poljar/weechat-matrix"
SRC_URI="https://github.com/poljar/${PN}/archive/${PV}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"
RESTRICT="!test? ( test )"

RDEPEND="
	net-irc/weechat[python,${PYTHON_SINGLE_USEDEP}]
	$(python_gen_cond_dep '
		dev-python/pyopenssl[${PYTHON_USEDEP}]
		dev-python/webcolors[${PYTHON_USEDEP}]
		dev-python/future[${PYTHON_USEDEP}]
		dev-python/atomicwrites[${PYTHON_USEDEP}]
		dev-python/attrs[${PYTHON_USEDEP}]
		dev-python/logbook[${PYTHON_USEDEP}]
		dev-python/pygments[${PYTHON_USEDEP}]
		dev-python/matrix-nio[e2e,${PYTHON_USEDEP}]
		dev-python/aiohttp[${PYTHON_USEDEP}]
		dev-python/python-magic[${PYTHON_USEDEP}]
		dev-python/requests[${PYTHON_USEDEP}]
	')
"

BDEPEND="
	test? ( $(python_gen_cond_dep '
		dev-python/pytest[${PYTHON_USEDEP}]
	') )
"

DEPEND="
	test? (
		$(python_gen_cond_dep '
			dev-python/hypothesis[${PYTHON_USEDEP}]
		')
		${RDEPEND}
	)
"

src_compile() {
	: # no build
}

src_test() {
	PYTHONPATH=. pytest
}

src_install() {
	emake DESTDIR="${D}" PREFIX="/usr/share/weechat" install
	elog "In order to autoload ${PN} you need to run the following command:"
	elog "    ln -s /usr/share/weechat/python/matrix.py -t ~/.weechat/python/autoload/"
}
