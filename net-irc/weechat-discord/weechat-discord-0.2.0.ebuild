# Copyright 2017-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CRATES="
addr2line-0.12.1
adler32-1.0.4
aho-corasick-0.7.10
arrayref-0.3.6
arrayvec-0.5.1
autocfg-0.1.7
autocfg-1.0.0
backtrace-0.3.48
base64-0.10.1
base64-0.11.0
bindgen-0.53.2
bitflags-1.2.1
blake2b_simd-0.5.10
block-buffer-0.7.3
block-padding-0.1.5
bumpalo-3.3.0
byteorder-1.3.4
bytes-0.4.12
byte-tools-0.3.1
cc-1.0.53
cexpr-0.4.0
cfg-if-0.1.10
chrono-0.4.11
clang-sys-0.29.3
cloudabi-0.0.3
constant_time_eq-0.1.5
cookie-0.12.0
cookie_store-0.7.0
crc32fast-1.2.0
crossbeam-channel-0.4.2
crossbeam-deque-0.7.3
crossbeam-epoch-0.8.2
crossbeam-queue-0.2.1
crossbeam-utils-0.7.2
ct-logs-0.6.0
digest-0.8.1
dirs-2.0.2
dirs-sys-0.3.4
dtoa-0.4.5
either-1.5.3
encoding_rs-0.8.23
error-chain-0.12.2
failure-0.1.8
failure_derive-0.1.8
fake-simd-0.1.2
flate2-1.0.14
fnv-1.0.7
fuchsia-cprng-0.1.1
fuchsia-zircon-0.3.3
fuchsia-zircon-sys-0.3.3
futures-0.1.29
futures-cpupool-0.1.8
generic-array-0.12.3
getrandom-0.1.14
gimli-0.21.0
glob-0.3.0
h2-0.1.26
hermit-abi-0.1.13
http-0.1.21
httparse-1.3.4
http-body-0.1.0
hyper-0.12.35
hyper-rustls-0.17.1
idna-0.1.5
idna-0.2.0
indexmap-1.3.2
input_buffer-0.2.0
iovec-0.1.4
itoa-0.4.5
json-0.12.4
js-sys-0.3.39
kernel32-sys-0.2.2
lazycell-1.2.1
lazy_static-1.4.0
libc-0.2.70
lock_api-0.3.1
lock_api-0.3.4
log-0.4.8
matches-0.1.8
maybe-uninit-2.0.0
memchr-2.3.3
memoffset-0.5.4
mime-0.3.16
mime_guess-2.0.3
miniz_oxide-0.3.6
mio-0.6.22
miow-0.2.1
net2-0.2.34
nom-5.1.1
num_cpus-1.13.0
num-integer-0.1.42
num-traits-0.2.11
object-0.19.0
once_cell-1.4.0
onig-4.3.3
onig_sys-69.1.0
opaque-debug-0.2.3
parsing-0.1.0
peeking_take_while-0.1.2
percent-encoding-1.0.1
percent-encoding-2.1.0
pkg-config-0.3.17
ppv-lite86-0.2.8
proc-macro2-1.0.13
publicsuffix-1.5.4
quote-1.0.6
rand-0.6.5
rand-0.7.3
rand_chacha-0.1.1
rand_chacha-0.2.2
rand_core-0.3.1
rand_core-0.4.2
rand_core-0.5.1
rand_hc-0.1.0
rand_hc-0.2.0
rand_isaac-0.1.1
rand_jitter-0.1.4
rand_os-0.1.3
rand_pcg-0.1.2
rand_xorshift-0.1.1
rdrand-0.4.0
redox_syscall-0.1.56
redox_users-0.3.4
regex-1.3.7
regex-syntax-0.6.17
reqwest-0.9.24
ring-0.16.13
rust-argon2-0.7.0
rustc-demangle-0.1.16
rustc-hash-1.1.0
rustc_version-0.2.3
rustls-0.16.0
ryu-1.0.4
scopeguard-1.1.0
sct-0.6.0
semver-0.9.0
semver-parser-0.7.0
serde-1.0.110
serde_derive-1.0.110
serde_json-1.0.53
serde_urlencoded-0.5.5
sha-1-0.8.2
shlex-0.1.1
slab-0.4.2
smallvec-0.6.13
smallvec-1.4.0
spin-0.5.2
string-0.2.1
syn-1.0.22
synstructure-0.12.3
thread_local-1.0.1
threadpool-1.8.1
time-0.1.43
tokio-0.1.22
tokio-buf-0.1.1
tokio-current-thread-0.1.7
tokio-executor-0.1.10
tokio-io-0.1.13
tokio-reactor-0.1.12
tokio-rustls-0.10.3
tokio-sync-0.1.8
tokio-tcp-0.1.4
tokio-threadpool-0.1.18
tokio-timer-0.2.13
traitobject-0.1.0
try_from-0.3.2
try-lock-0.2.2
tungstenite-0.9.2
typemap-0.3.3
typenum-1.12.0
unicase-2.6.0
unicode-bidi-0.3.4
unicode-normalization-0.1.12
unicode-xid-0.2.0
unsafe-any-0.4.2
untrusted-0.7.1
url-1.7.2
url-2.1.1
utf-8-0.7.5
uuid-0.7.4
version_check-0.9.1
want-0.2.0
wasi-0.9.0+wasi-snapshot-preview1
wasm-bindgen-0.2.62
wasm-bindgen-backend-0.2.62
wasm-bindgen-macro-0.2.62
wasm-bindgen-macro-support-0.2.62
wasm-bindgen-shared-0.2.62
webpki-0.21.2
webpki-roots-0.17.0
web-sys-0.3.39
winapi-0.2.8
winapi-0.3.8
winapi-build-0.1.1
winapi-i686-pc-windows-gnu-0.4.0
winapi-x86_64-pc-windows-gnu-0.4.0
winreg-0.6.2
ws2_32-sys-0.2.1
pcre2-0.2.3
pcre2-sys-0.2.2
parking_lot-0.6.2
parking_lot_core-0.6.2
"

inherit cargo

DESCRIPTION="Weechat plugin for Discord support"
# Double check the homepage as the cargo_metadata crate
# does not provide this value so instead repository is used
HOMEPAGE="https://github.com/terminal-discord/weechat-discord"
SRC_URI="
	$(cargo_crate_uris ${CRATES})
	https://github.com/terminal-discord/${PN}/archive/master.zip
	https://github.com/terminal-discord/serenity/archive/8d274332.tar.gz -> serenity-8d274332b62632876c7ff73b72d1fc21de8e257d.crate
	https://github.com/Noskcaj19/simple-ast/archive/7b9d765376c8adcc11384bb5cd64a61dd74b1513.tar.gz -> simple-ast-7b9d765376c8adcc11384bb5cd64a61dd74b1513.crate
	https://github.com/terminal-discord/rust-weechat/archive/39219d7.tar.gz -> rust-weechat-39219d770f1684a3ff05bf7853c87bd019592f9e.crate
	https://github.com/terminal-discord/parking_lot/archive/046a171.tar.gz -> parking_lot-046a17142065ea47e2ae8073ba1fd55a4ce9efdd.crate
"

RESTRICT="mirror"
# License set may be more restrictive as OR is not respected
# use cargo-license for a more accurate license picture
LICENSE="Apache-2.0 Apache-2.0 WITH LLVM-exception BSD-2-Clause BSD-3-Clause BSL-1.0 CC0-1.0 ISC MIT MPL-2.0 Unlicense Zlib"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

S="${WORKDIR}/${PN}-master"

BDEPEND="net-irc/weechat"
RDEPEND="${BDEPEND}"


src_prepare() {
	cat >> ${ECARGO_HOME}/config <<-EOF
		[source."https://github.com/Noskcaj19/simple-ast"]
		git = "https://github.com/Noskcaj19/simple-ast"
		rev = "7b9d765"
		replace-with = "gentoo"

		[source."https://github.com/terminal-discord/parking_lot"]
		git = "https://github.com/terminal-discord/parking_lot"
		rev = "046a171"
		replace-with = "gentoo"

		[source."https://github.com/terminal-discord/rust-weechat"]
		git = "https://github.com/terminal-discord/rust-weechat"
		rev = "39219d7"
		replace-with = "gentoo"

		[source."https://github.com/terminal-discord/serenity"]
		git = "https://github.com/terminal-discord/serenity"
		rev = "8d274332"
		replace-with = "gentoo"

		[patch.crates-io]
		parking_lot = { rev = "046a171", git = "https://github.com/terminal-discord/parking_lot"}
	EOF
	for package in rust-weechat-39219d770f1684a3ff05bf7853c87bd019592f9e/weechat-{rs,sys,macro}; do
		cp -r ${ECARGO_HOME}/gentoo/${package} ${ECARGO_HOME}/gentoo/
		echo '{ "package": "", "files": {} }' > ${ECARGO_HOME}/gentoo/$(basename ${package})/.cargo-checksum.json
	done

	cat >> Cargo.toml <<-EOF
		[patch.crates-io]
		parking_lot = { rev = "046a171", git = "https://github.com/terminal-discord/parking_lot"}
	EOF
	default
}

src_install() {
	insinto /usr/$(get_libdir)/weechat/plugins
	doins target/release/libweecord.so
}
