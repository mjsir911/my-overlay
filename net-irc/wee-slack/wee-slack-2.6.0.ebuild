# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
PYTHON_COMPAT=( python3_{6,7} )

inherit python-single-r1

DESCRIPTION="A WeeChat script for Slack.com"
HOMEPAGE="https://github.com/wee-slack/wee-slack"
SRC_URI="https://github.com/${PN}/${PN}/archive/v${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"
RESTRICT="!test? ( test )"


RDEPEND="
	net-irc/weechat[python,${PYTHON_SINGLE_USEDEP}]
	$(python_gen_cond_dep '
		dev-python/websocket-client[${PYTHON_USEDEP}]
	')
"

BDEPEND="
	test? ( $(python_gen_cond_dep '
		dev-python/pytest[${PYTHON_USEDEP}]
	') )
"

DEPEND="
	test? (
		$(python_gen_cond_dep '
			dev-python/mock[${PYTHON_USEDEP}]
		')
		${RDEPEND}
	)
"

src_test() {
	TZ=UTC pytest
}

src_install() {
	insinto /usr/share/weechat/python/
	doins wee_slack.py
	elog "In order to autoload ${PN} you need to run the following command:"
	elog "    ln -s /usr/share/weechat/python/wee_slack.py -t ~/.weechat/python/autoload/"
}

